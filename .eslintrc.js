module.exports = {
  env: {
    node: true,
  },
  extends: ["@vue/airbnb", "@vue/typescript/recommended"],
  overrides: [
    {
      env: {
        jest: true,
      },
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)",
      ],
    },
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  root: true,
  rules: {
    "@typescript-eslint/camelcase": "off",
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
  },
};
