import * as L from 'leaflet';
declare module 'leaflet' {
  class vectorGrid {
    constructor();
    static slicer(geojson: L.GeoJSON, options: {}): any;
  }

  interface ProjectionOptions extends ControlOptions{

  }

  interface Layer {
    feature?: any;
  }

  interface PolylineOptions {
    id?: number;
  }

  class polylineDecorator extends Layer {
    constructor(latLng: number[] | Polyline | Polygon, options: PolylineOptions)
  }
  /**
   * GEOMAN PLUGIN
   */
  type position = 'topleft' | 'topright' | 'bottomleft' | 'bottomright';
  interface GeomanControlOptions extends L.Control {
    position: position,
    drawMarker: boolean,
    drawCircleMarker: boolean,
    drawPolyline: boolean,
    drawRectangle: boolean,
    drawPolygon: boolean,
    drawCircle: boolean,
    editMode: boolean,
    dragMode: boolean,
    cutPolygon: boolean,
    removalMode: boolean,
  }

  interface Map {
    pm: any;
    _controlContainer: HTMLElement;
    getBearing();
    setBearing(latlng: L.LatLng);
  }

  /**
   * SIMPLE MAP SCREENSHOOTER
   */
  namespace control {
    function groupedLayers(any: {}, featureGroup: L.FeatureGroup, options: {});
    function browserPrint(options?: any): Control.BrowserPrint;
    function takeScreen(format: any, overridedPluginOptions: any): simpleMapScreenshoter;
    function projection();

    interface simpleMapScreenshoter {
      addTo(map: L.Map): any;
    }
  }

  /**
   * GEOMETRYUTIL
   */

  export class GeometryUtil {
    static computeAngle(a: L.Point, b: L.Point): number;
    static closest(map: L.Map, layer: L.Layer, latlng: L.LatLng, vertices?: boolean): L.LatLng;
    static closestOnSegment(map: Map, a: L.LatLng, b: L.LatLng, c: L.LatLng);
    static length(arr: LatLng[])
  }

  namespace Control {
    interface BrowserPrint {
      addTo(map: L.Map): any;
    }

    class MiniMap extends Control {
      constructor(featureGroup: L.FeatureGroup, options: {});
    }
  }
}
