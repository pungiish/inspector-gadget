import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-inspect-issue',
  templateUrl: './inspect-issue.component.html',
  styleUrls: ['./inspect-issue.component.scss']
})
export class InspectIssueComponent implements OnChanges {
  @Input() createdElement: any;

  @Output() toggle = new EventEmitter();

  fileIssueForm = this.fb.group({
    verified: [''],
    observation: [''],
    undefined: [''],
  });

  constructor(
    private fb: FormBuilder
  ) { }


  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'createdElement': {
            console.log(this.createdElement);
          }
        }
      }
    }
  }

  onSubmit() { }

  onToggle() {
    this.toggle.emit();
  }

}
