import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectIssueComponent } from './inspect-issue.component';

describe('InspectIssueComponent', () => {
  let component: InspectIssueComponent;
  let fixture: ComponentFixture<InspectIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
