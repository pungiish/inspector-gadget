import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineIssueComponent } from './define-issue.component';

describe('DefineIssueComponent', () => {
  let component: DefineIssueComponent;
  let fixture: ComponentFixture<DefineIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
