import { Component, OnInit, OnChanges, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiProjectDataService } from 'shared/services/api-data.service';

@Component({
  selector: 'app-set-discipline',
  templateUrl: './define-issue.component.html',
  styleUrls: ['./define-issue.component.scss']
})
export class DefineIssueComponent implements OnInit, OnChanges {

  @Input() createdElement: any;

  @Output() submitElement = new EventEmitter();
  @Output() updateElement = new EventEmitter();
  @Output() toggle = new EventEmitter();


  disciplines: any = [];
  subdisciplines: any = [];
  // classifications: any;
  data: any;

  insertOrUpdate = 'Insert';

  fileIssueForm = this.fb.group({
    discipline: ['', Validators.required],
    subdiscipline: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private projectDataService: ApiProjectDataService,
  ) {
    this.data = this.projectDataService.dataArr;
    this.data.forEach(element => {
      this.disciplines.push(element.trade_name);
    });
  }

  ngOnInit() {
  }

  getSubdiscipline() {
    return (this.data.find(x => x.trade_name === this.fileIssueForm.controls.discipline.value).subdisciplines);
  }

  onSubmit() {
    if (this.fileIssueForm.valid) {
      if (this.insertOrUpdate === 'Update') {
        this.onUpdate();
        this.createdElement = null;
        this.fileIssueForm.reset();
        this.fileIssueForm.markAsPristine();
        return;
      }
      console.log(this.createdElement);
      let coords;
      if (this.createdElement.shape === 'Line') {
        coords = this.createdElement.layer._rings[0].map((point) => {
          return [point.x, point.y];
        });
      } else if (this.createdElement.shape === 'Marker') {
        coords = [this.createdElement.layer._latlng.lat, this.createdElement.layer._latlng.lng];
      }
      this.submitElement.emit({
        _id: this.createdElement.layer._leaflet_id,
        discipline: this.fileIssueForm.controls.discipline.value,
        subdiscipline: this.fileIssueForm.controls.subdiscipline.value,
        // classification: this.fileIssueForm.controls.classification.value,
        coordinates: coords,
        layer: this.createdElement.layer
      });
    }
    this.fileIssueForm.reset();
    this.fileIssueForm.markAsPristine();
  }

  onUpdate() {
    this.updateElement.emit({
      id: this.createdElement.id,
      discipline: this.fileIssueForm.controls.discipline.value,
      subdiscipline: this.fileIssueForm.controls.subdiscipline.value,
      // classification: this.fileIssueForm.controls.classification.value
    });

  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'createdElement': {
            console.log(this.createdElement);
            if (!!this.createdElement && this.createdElement.hasOwnProperty('element')) {
              this.insertOrUpdate = 'Update';
              this.fileIssueForm.controls.discipline.setValue(this.createdElement.element.discipline);
              this.fileIssueForm.controls.subdiscipline.setValue(this.createdElement.element.subdiscipline);
              // this.fileIssueForm.controls.classification.setValue(this.createdElement.element.classification);
            } else {
              this.insertOrUpdate = 'Insert';
              this.fileIssueForm.markAsPristine();
              this.fileIssueForm.markAsUntouched();
              this.fileIssueForm.reset();
              this.fileIssueForm.controls.discipline.setValue('Plumbing');
              this.fileIssueForm.controls.subdiscipline.setValue('Risers');
            }
          }
        }
      }
    }
  }

  onToggle() {
    this.toggle.emit();
  }

}
