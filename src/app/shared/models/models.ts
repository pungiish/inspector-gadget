import { HttpProgressEvent } from '@angular/common/http';

export interface IGeometry {
  _id?: string;
  type: string;
  coordinates: number[];
  issueIds?: string[];
  checked?: boolean;
  geometries?: IGeometry[];
}

export interface IGeometryCollection extends IGeometry {
  type: string;
  geometries: IGeometry[];
}

export interface IProperties {
  Layer: string;
  SubClasses: string;
  ExtendedEntity: {};
  LineType: string;
  EntityHandle: string;
  Text: string;
  issue: Issue;
  value: string;
  rotationAngle: number;
}

export interface IGeoJson {
  type: string;
  properties: IProperties;
  geometry: IGeometry | IGeometryCollection;
}

export interface IFeatureCollection {
  type: string;
  features: IGeoJson[];
}
export interface ILayer {
  [key: string]: IGeoJson[];
}

export interface Issue {
  _id: number;
  discipline: string;
  subdiscipline: string;
  verification?: Verification | string;
  coordinates?: { lat: number, lng: number; };
  accept: boolean;
}

export interface Verification {
  classification: string;
  subclassification: string;
  impact?: string;
  measurement?: number;
  comment?: string;
}

export interface APIResponse {
  status: string;
  message: any;
}

export type Severity = 'H' | 'M' | 'L';

export class  CsvTxtPoint {
  layer: string;
  rotation: string;
  posX: string;
  posY: string;
  value: string;
}

class Geometry {
  type = 'Point';
  coordinates: number[];
}

class Properties {
  Layer: string;
  value: string;
  rotationAngle: number;
}

export class Feature {
  type = 'Feature';
  properties: Properties = new Properties();
  geometry: Geometry = new Geometry();
}

  // export class Feature {
  //   type: 'Feature';
  //   properties: {
  //     name: string;
  //   };
  //   geometry: {
  //     type: 'Point';
  //     coordinates: number[];
  //   };
  // }
