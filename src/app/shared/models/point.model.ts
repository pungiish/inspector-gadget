import { LatLng } from 'leaflet';

export class Point {
  private lat: number;
  private lng: number;
  private precision: number;
  private distance?: number;
  private coords?: Point;
  constructor(point: { lat, lng; }, distance?, coords?) {
    this.precision = 1;
    this.lat = point.lat * this.precision;
    this.lng = point.lng * this.precision;
    if (!!distance) {
      this.distance = distance;
    }
    if (!!coords) {
      if (!!coords.length) {
        this.coords = new Point(coords[0], coords[1]);
      }
    }
  }

  private _setPrecision() {
    return this.precision;
  }

  getCoords() {
    return `lat: ${this.getCoords().getDividedLat}, lng: ${this.getCoords().getDividedLng}`;
  }

  getDistance() {
    return this.distance;
  }

  getPrecision() {
    return this.precision;
  }
  getLat() {
    return this.lat;
  }
  getLng() {
    return this.lng;
  }
  getDividedLat() {
    return this.lat / this.precision;
  }
  getDividedLng() {
    return this.lng / this.precision;
  }
  getLatLng(): { lat, lng; } {
    return { lat: this.lat, lng: this.lng };
  }
  getDividedLatLng(): LatLng {
    return new LatLng(this.lat / this.precision, this.lng / this.precision);
  }
}
