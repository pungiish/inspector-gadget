import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ApiProjectDataService } from './services/api-data.service';
import { DefineIssueComponent } from './components/define-issue/define-issue.component';
import { ReactiveFormsModule } from '@angular/forms';

import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { InspectIssueComponent } from './components/inspect-issue/inspect-issue.component';
import { CsvPointRecordService } from './services/csvPointRecord.service';

const MATERIAL_MODULES = [
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressBarModule,
  MatSelectModule,
  MatButtonModule,
  MatIconModule
];
@NgModule({
  declarations: [DefineIssueComponent, InspectIssueComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    MATERIAL_MODULES,
  ],
  exports: [
    CommonModule,
    DefineIssueComponent,
    InspectIssueComponent,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ApiProjectDataService, CsvPointRecordService]
    };
  }
}
