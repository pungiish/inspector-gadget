import { IGeoJson, ILayer, IGeometryCollection } from '../models/models';


declare global {
  interface Array<T> {
    groupBy(o: string, p: string): Array<ILayer>;
  }
}

Array.prototype.groupBy = function (prop: string, prop2?: string): ILayer[] {
  return this.reduce((array: any, feature) => {
    if (!feature.geometry) {
      return array;
    }
    if (feature.properties.value) {
      console.log(feature)
    }
    const layerName = feature[prop][prop2];
    let groupedLayerName = layerName.split('$')[0];
    // If $ is at the beginning
    if (groupedLayerName === '') {
      groupedLayerName = '$';
    }
    array[groupedLayerName] = array[groupedLayerName] || {};
    array[groupedLayerName][layerName] = array[groupedLayerName][layerName] || [];
    array[groupedLayerName][layerName].push(feature);
    return array;
  }, {});
};
