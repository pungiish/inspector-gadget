import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { APIResponse } from 'shared/models/models';
import { FeatureGroup } from 'leaflet';

export interface ProjectDataService {
  layersControl: { baseLayers: {}, groupedLayers: {}; };
  minimaplayersControl: { baseLayers: {}, overlays: {}; };
  projectForm: any;
  geoJSON: any;
  transformedLayers: any;
  elements: any;
  isLoading$: BehaviorSubject<boolean>;
  toGeoJson(formData: FormData): void;
  readGeoJson(url: string): Observable<APIResponse>;
  getDisciplines(): Observable<Map<any, any>>;
  getSubDisciplines(): Observable<Map<any, any>>;
  getClassifications(): Observable<Map<any, any>>;
  getSubClassifications(): Observable<Map<any, any>>;
  getImpacts(): Observable<Map<any, any>>;
  getFullData(): Observable<Map<any, any>>;
  resetData(): void;
}
