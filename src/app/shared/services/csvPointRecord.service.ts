import { CsvTxtPoint, Feature } from '../models/models';
import proj4 from "proj4";

export class CsvPointRecordService {
  records: any[] = [];
  recLength: number;
  featureCollection: Feature[] = [];

  uploadListener(csvData: any): void {
    const csvRecordsArray = csvData
    console.log(csvRecordsArray);
    this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray);
    this.createFeatures();
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any) {
    const csvArr = [];
    csvRecordsArray.filter(record => {
      return !!record.Layer ? true : false
    }).map(record => {
      const csvTxtPoint: CsvTxtPoint = new CsvTxtPoint();
      csvTxtPoint.layer = record.Layer
      csvTxtPoint.posX = (record['Position X'] as string).replace(',', '.');
      csvTxtPoint.posY = (record['Position Y'] as string).replace(',', '.');
      csvTxtPoint.rotation = record.Rotation
      csvTxtPoint.value = record.Value
      csvArr.push(csvTxtPoint);
    })
    return csvArr;
  }
  fileReset(csvReader) {
    csvReader.nativeElement.value = '';
    this.records = [];
  }

  createFeatures() {
    this.records.map(record => {
      if (record.posX) {
        let feature: Feature = new Feature();
        feature.properties.value = record.value;
        feature.properties.rotationAngle = Number(record.rotation);
        feature.properties.Layer = record.layer;
        proj4.defs('EPSG:2278', '+proj=lcc +lat_1=30.28333333333333 +lat_2=28.38333333333333 +lat_0=27.83333333333333 +lon_0=-99 +x_0=600000 +y_0=3999999.9998984 +ellps=GRS80 +datum=NAD83 +to_meter=0.3048006096012192 +no_defs');
        let projectedCoords = proj4('EPSG:2278', proj4.WGS84, [Number(record.posX), Number(record.posY)])
        feature.geometry.coordinates = projectedCoords
        this.featureCollection.push(feature);
      }
    })
  }
}

