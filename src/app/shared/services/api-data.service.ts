import { Injectable } from '@angular/core';
import { ProjectDataService } from './data.service';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { APIResponse, IFeatureCollection } from 'shared/models/models';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ApiProjectDataService implements ProjectDataService {

  constructor(
    private httpClient: HttpClient,
    private router: Router) {
    this.getFullData().subscribe(x => {
      this.IssueClassificationData = x;
      Object.keys(this.IssueClassificationData).forEach(key => {
        this.dataArr.push(this.IssueClassificationData[key]);
      });
    });
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa('jan.pungarsek@proxima.si:12345678')
    })
  };
  minimaplayersControl: any = { baseLayers: {}, overlays: {} };
  layersControl: { baseLayers: {}, groupedLayers: {}; };
  projectForm: any;
  geoJSON: any;
  transformedLayers: any;
  elements: any;
  isLoading$ = new BehaviorSubject(false);
  IssueClassificationData;
  dataArr = [];

  toGeoJson(formData: FormData) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    return this.httpClient.post(environment.url, formData, {
      headers,
      reportProgress: true,
      observe: 'events'
    })
      .pipe(
        map((event) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              if (!!event.total) {
                const progress = Math.round(100 * event.loaded / event.total);
                const APIProgress: APIResponse = { status: 'PROGRESS', message: progress };
                return APIProgress;
              } else {
                const APIUnknown: APIResponse = { status: '???', message: '???' };
                return APIUnknown;
              }
            case HttpEventType.Response:
              const APIResponse: APIResponse = { status: 'COMPLETED', message: event.body };
              return APIResponse;
            case HttpEventType.DownloadProgress:
              const APIDownloading: APIResponse = { status: 'DOWNLOADING', message: event };
              return APIDownloading;
            default:
              return `UNHANDLED EVENT: ${JSON.stringify(event)}`;
          }
        })
      );
  }

  readGeoJson(url: string): Observable<APIResponse> {
    return this.httpClient.get(url)
      .pipe(
        map(res => {
          const APIResponse: APIResponse = { status: 'COMPLETED', message: res as IFeatureCollection };
          return APIResponse;
        })
      );
  }


  getDisciplines(): Observable<Map<any, any>> {
    return this.httpClient.get<Map<any, any>>(environment.serverUrl + `/inspection/points/disciplines/`);
  }

  getSubDisciplines(): Observable<Map<any, any>> {
    return this.httpClient.get<Map<any, any>>(environment.serverUrl + `/inspection/points/subdisciplines/`);

  }

  getClassifications(): Observable<Map<any, any>> {
    return this.httpClient.get<Map<any, any>>(environment.serverUrl + `/inspection/issues/classifications/`);
  }

  getSubClassifications(): Observable<Map<any, any>> {
    return this.httpClient.get<Map<any, any>>(environment.serverUrl + `/inspection/issues/subclassifications/`);
  }

  getImpacts(): Observable<Map<any, any>> {
    return this.httpClient.get<Map<any, any>>(environment.serverUrl + `/inspection/issues/impacts/`);
  }

  getFullData(): Observable<Map<any, any>> {
    return this.httpClient.get<Map<any, any>>(environment.serverUrl + `/inspection/report_type/1/full_data`, this.httpOptions);
  }

  resetData(): void {
    this.elements = null;
  }
}
