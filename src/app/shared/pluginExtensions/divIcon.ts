
import {DivIcon } from 'leaflet'
// @ts-ignore
DivIcon.customDivIconColor = DivIcon.extend({
    createIcon: function(oldIcon) {
      var icon = DivIcon.prototype.createIcon.call(this, oldIcon);
      icon.style.color = this.options.color;
      return icon;
  }
  })