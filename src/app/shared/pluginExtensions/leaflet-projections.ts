import { DomUtil, DomEvent, layerGroup, featureGroup, circleMarker, polyline, Control, control, LatLng, Util } from 'leaflet';
import { Point } from '../models/point.model';
import * as L from 'leaflet';
import { MatSnackBar } from '@angular/material/snack-bar';


/**
 * Earth Radius used with the Harvesine formula and approximates using a spherical (non-ellipsoid) Earth.
 */
export const earthRadius = 6371008.8;

/**
 * Units of measurement factors based on 1 meter.
 */
export const unitsFactors = {
  meters: 1,
  metres: 1,
  millimeters: 1000,
  millimetres: 1000,
  centimeters: 100,
  centimetres: 100,
  kilometers: 1 / 1000,
  kilometres: 1 / 1000,
  miles: 1 / 1609.344,
  nauticalmiles: 1 / 1852,
  inches: 39.370,
  yards: 1 / 1.0936,
  feet: 3.28084,
  radians: 1 / earthRadius,
  degrees: 1 / 111325,
};

export class Projection extends Control {
  options = {
    position: ("topleft" as L.ControlPosition),
    circleMarker: {
      color: 'red',
      radius: 2
    },
    lineStyle: {
      color: 'red',
      dashArray: '1,6'
    },
    lengthUnit: {
      display: 'm',
      decimal: 4,
      factor: 1000, // from km to m
      label: 'Distance:'
    },
    angleUnit: {
      display: '&deg;',
      decimal: 2,
      factor: 0,
      label: 'Bearing:'
    }
  };
  _map: L.Map;
  _container: HTMLElement
  _choice: boolean;
  _defaultCursor;
  _allLayers;
  _clickedLatLong;
  _movingLatLong;
  _clickedPoints;
  _totalLength;
  _clickCount;
  _tempLine;
  _tempPoint;
  _result;
  _pointLayer;
  _polylineLayer;
  _origin;
  _pointOnXAxis;
  _pointOnYAxis;
  _distance;
  _axesArray: { xAxis, yAxis };
  snackbarDuration = 700;

  initialize(options) {
    Util.setOptions(this, options);
    // Continue initializing the control plugin here.
  }
  constructor(private snackbar: MatSnackBar, options?: L.ControlOptions) {
    super(options);
  }

  onAdd(map) {
    this._map = map;
    this._container = DomUtil.create('button', 'toggleElements mat-icon-button mat-button-base', this._container); // DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-load');
    //let button = DomUtil.create('button', 'toggleElements', this._container);
    let matIcon = DomUtil.create('mat-icon', 'mat-icon material-icons', this._container);
    this._container.setAttribute('mat-icon-button', '');
    this._container.setAttribute('role', 'button');
    this._container.setAttribute('aria-label', 'Measure');
    this._container.setAttribute('matTooltip', 'Measure');
    this._container.setAttribute('matTooltipPosition', 'right');
    this._container.setAttribute('matTooltipClass', 'rotate-tooltip');
    matIcon.innerText = 'square_foot';
    this._container.title = 'Measure';
    // DomEvent.disableClickPropagation(this._container);
    DomEvent.on(this._container, 'click', this._toggleMeasure, this);
    this._choice = false;
    this._defaultCursor = this._map.getContainer().style.cursor;
    this._allLayers = layerGroup();
    return this._container;
  }

  onRemove() {
    DomEvent.off(this._container, 'click', this._toggleMeasure, this);
  }

  _toggleMeasure() {
    console.log(this._choice)
    this._choice = !this._choice;
    this._clickedLatLong = null;
    this._clickedPoints = [];
    this._totalLength = 0;
    if (this._choice) {
      this._map.doubleClickZoom.disable();
      DomEvent.on(this._map.getContainer(), 'keydown', this._escape, this);
      DomEvent.on(this._map.getContainer(), 'dblclick', this._closePath, this);
      this._container.classList.add("leaflet-ruler-clicked");
      this._clickCount = 0;
      this._tempLine = featureGroup().addTo(this._allLayers);
      this._tempPoint = featureGroup().addTo(this._allLayers);
      this._pointLayer = featureGroup().addTo(this._allLayers);
      this._polylineLayer = featureGroup().addTo(this._allLayers);
      this._allLayers.addTo(this._map);
      this._map.getContainer().style.cursor = 'crosshair';
      this._map.on('click', this._clicked, this);
      this._map.on('mousemove', this._moving, this);
      this.snackbar.open('Measuring enabled', '', {
        duration: this.snackbarDuration,
        horizontalPosition: 'center',
        verticalPosition: 'top',
        panelClass: ['snackbar-primary']
      })
    }
    else {
      this._map.doubleClickZoom.enable();
      DomEvent.off(this._map.getContainer(), 'keydown', this._escape, this);
      DomEvent.off(this._map.getContainer(), 'dblclick', this._closePath, this);
      this._container.classList.remove("leaflet-ruler-clicked");
      this._map.removeLayer(this._allLayers);
      this._allLayers = layerGroup();
      this._map.getContainer().style.cursor = this._defaultCursor;
      this._map.off('click', this._clicked, this);
      this._map.off('mousemove', this._moving, this);
      this.snackbar.open('Measuring disabled', '', {
        duration: this.snackbarDuration,
        horizontalPosition: 'center',
        verticalPosition: 'top',
        panelClass: ['snackbar-warn']
      })
    }
  }
  _clicked(e) {
    this._clickedLatLong = e.latlng;
    this._clickedPoints.push(this._clickedLatLong);
    circleMarker(this._clickedLatLong, this.options.circleMarker).addTo(this._pointLayer);
    if (this._clickCount === 0) {
      this._origin = new Point(e.latlng);
    }
    // If it's not the first click and not the same point was clicked.
    if (this._clickCount > 0 && !e.latlng.equals(this._clickedPoints[this._clickedPoints.length - 2])) {
      if (this._movingLatLong) {
        if (this._clickCount === 2) {
          // show projected points on both axes
          let xPerp = L.GeometryUtil.closestOnSegment(this._map, this._movingLatLong, (this._origin as Point).getDividedLatLng(), (this._pointOnXAxis as Point).getDividedLatLng());
          let yPerp = L.GeometryUtil.closestOnSegment(this._map, this._movingLatLong, (this._origin as Point).getDividedLatLng(), (this._pointOnYAxis as Point).getDividedLatLng());
          this._distance = this._result.Distance;
          let text = '<b>' + this.options.lengthUnit.label + '</b>&nbsp;' + this._result.Distance.toFixed(this.options.lengthUnit.decimal) + '&nbsp;' + this.options.lengthUnit.display + '<br/> <b>' + 'distance: ' + this._distance + '</b>';
          polyline([(this._origin as Point).getDividedLatLng(), this._movingLatLong], this.options.lineStyle).addTo(this._polylineLayer);
          circleMarker(this._movingLatLong, this.options.circleMarker).bindTooltip(text, { sticky: true, offset: [0, -20], className: 'moving-tooltip' }).addTo(this._pointLayer).openTooltip();

        } else {
          polyline([this._clickedPoints[this._clickCount - 1], this._movingLatLong], this.options.lineStyle).addTo(this._polylineLayer);
        }
      }
      if (this._clickCount === 1) {
        // calculate XAxis and add it to the _polylineLayer
        this._pointOnXAxis = this._calculateXAxis(new Point(this._clickedLatLong), 10);
        let xAxis = polyline([(this._origin as Point).getDividedLatLng(), this._pointOnXAxis.getDividedLatLng()], this.options.lineStyle).addTo(this._polylineLayer);
        let yAxis = polyline([(this._origin as Point).getDividedLatLng(), this._calculateNegativeVector(this._pointOnXAxis).getDividedLatLng()], this.options.lineStyle).addTo(this._polylineLayer);
        this._axesArray = { xAxis: [this._pointOnXAxis.getDividedLatLng(), this._calculateNegativeVector(this._pointOnXAxis).getDividedLatLng()], yAxis: [(this._origin as Point).getDividedLatLng(), this._movingLatLong] };
      }
    }
    this._clickCount++;
    if (this._clickCount === 3) {
      this._origin = null;
      this._closePath();
    }
  }
  _moving(e) {
    if (this._clickedLatLong) {
      DomEvent.off(this._container, 'click', this._toggleMeasure, this);
      this._movingLatLong = e.latlng;
      // Odstrani prejšnje tempLine
      if (this._tempLine) {
        this._map.removeLayer(this._tempLine);
        this._map.removeLayer(this._tempPoint);
      }
      let text;
      this._tempLine = featureGroup();
      this._tempPoint = featureGroup();
      this._tempLine.addTo(this._map);
      this._tempPoint.addTo(this._map);
      this._calculateBearingAndDistance();
      if (this._clickCount === 1) {
        // Show temp x axes
        this._pointOnYAxis = new Point(this._movingLatLong);
        this._pointOnXAxis = this._calculateXAxis(this._pointOnYAxis, 10);
        polyline([(this._origin as Point).getDividedLatLng(), this._pointOnXAxis.getDividedLatLng()], this.options.lineStyle).addTo(this._tempLine);
        polyline([(this._origin as Point).getDividedLatLng(), this._calculateNegativeVector(this._pointOnXAxis).getDividedLatLng()], this.options.lineStyle).addTo(this._tempLine);

      }
      if (this._clickCount === 2) {
        // show projected points on both axes
        let xPerp = new Point(L.GeometryUtil.closestOnSegment(this._map, this._movingLatLong, (this._origin as Point).getDividedLatLng(), (this._pointOnXAxis as Point).getDividedLatLng()));
        let yPerp = new Point(L.GeometryUtil.closestOnSegment(this._map, this._movingLatLong, (this._origin as Point).getDividedLatLng(), (this._pointOnYAxis as Point).getDividedLatLng()));
        // let len = this._calculateLength(this._origin, new Point(this._movingLatLong));
        let len = this._map.distance((this._origin as Point).getDividedLatLng(), new Point(this._movingLatLong).getDividedLatLng());
        this._distance = len;
        text = '<b>' + this.options.lengthUnit.label + '</b>&nbsp;' + this._result.Distance.toFixed(this.options.lengthUnit.decimal) + '&nbsp;' + this.options.lengthUnit.display + '<br/> <b>' + 'projected distance: ' + len + '</b>';
        circleMarker(this._movingLatLong, this.options.circleMarker).bindTooltip(text, { sticky: true, offset: [0, -20], className: 'moving-tooltip' }).addTo(this._tempPoint).openTooltip();
        circleMarker(xPerp.getDividedLatLng(), this.options.circleMarker).addTo(this._tempPoint);
        circleMarker(yPerp.getDividedLatLng(), this.options.circleMarker).addTo(this._tempPoint);
        polyline([(this._origin as Point).getDividedLatLng(), this._movingLatLong], this.options.lineStyle).addTo(this._tempLine);
      } else {
        polyline([this._clickedLatLong, this._movingLatLong], this.options.lineStyle).addTo(this._tempLine);
      }
    }
  }
  _escape(e) {
    if (e.keyCode === 27) {
      if (this._clickCount > 0) {
        this._closePath();
      }
      else {
        this._choice = true;
        this._toggleMeasure();
      }
    }
  }
  _calculateBearingAndDistance() {
    var f1 = (this._origin as Point).getDividedLat(), l1 = (this._origin as Point).getDividedLng(), f2 = this._movingLatLong.lat, l2 = this._movingLatLong.lng;
    var toRadian = Math.PI / 180;
    // haversine formula
    // bearing
    var y = Math.sin((l2 - l1) * toRadian) * Math.cos(f2 * toRadian);
    var x = Math.cos(f1 * toRadian) * Math.sin(f2 * toRadian) - Math.sin(f1 * toRadian) * Math.cos(f2 * toRadian) * Math.cos((l2 - l1) * toRadian);
    var brng = Math.atan2(y, x) * ((this.options.angleUnit.factor ? this.options.angleUnit.factor / 2 : 180) / Math.PI);
    brng += brng < 0 ? (this.options.angleUnit.factor ? this.options.angleUnit.factor : 360) : 0;
    // distance
    var R = this.options.lengthUnit.factor ? 6371.0088 * this.options.lengthUnit.factor : 6371.0088;
    var deltaF = (f2 - f1) * toRadian;
    var deltaL = (l2 - l1) * toRadian;
    var a = Math.sin(deltaF / 2) * Math.sin(deltaF / 2) + Math.cos(f1 * toRadian) * Math.cos(f2 * toRadian) * Math.sin(deltaL / 2) * Math.sin(deltaL / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var distance = R * c;
    this._result = {
      Bearing: brng,
      Distance: distance
    };
  }
  _calculateLength(point: Point, point2: Point) {
    let length = Math.sqrt(Math.pow(point.getDividedLng() - point2.getDividedLng(), 2.0) + Math.pow(point.getDividedLat() - point2.getDividedLat(), 2.0));
    return length;
  }
  _calculateXAxis(point: Point, scale: number = 10) {

    let yPoint = point;
    let x2 = (this._origin as Point).getDividedLng(), y2 = (this._origin as Point).getDividedLat(),
      x1 = point.getDividedLng(), y1 = point.getDividedLat();

    /**
     * The difference between the two vectors
     */
    let lat = x1 - x2;
    let lng = y1 - y2;
    /**
     * The angle in radians (in [-π,π]) between the positive x-axis and the ray from (0,0) to the point
     */
    let aTan = Math.atan2(lng, lat) + (Math.PI / 2);

    return new Point({ lat: (y2 + Math.sin(aTan) * scale), lng: (x2 + Math.cos(aTan) * scale) });
  }
  _calculateNegativeVector(vector: Point) {
    let deltaLat = vector.getDividedLat() - this._origin.getDividedLat();
    let deltaLng = vector.getDividedLng() - this._origin.getDividedLng();
    let lat = this._origin.getDividedLat() - deltaLat;
    let lng = this._origin.getDividedLng() - deltaLng;

    return new Point({ lat, lng });
  }
  _closePath() {
    this._map.removeLayer(this._tempLine);
    this._map.removeLayer(this._tempPoint);
    this._map.removeLayer(this._pointLayer);
    this._map.removeLayer(this._polylineLayer);
    this._choice = true;
    DomEvent.on(this._container, 'click', this._toggleMeasure, this);
    this._toggleMeasure();
  }
};