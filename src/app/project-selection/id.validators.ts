import { AbstractControl, ValidationErrors } from '@angular/forms';

export class IdValidators {
  static IsPlanIdExist(control: AbstractControl): Promise<ValidationErrors | null> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value as string === '00000') {
          resolve({ IsPlanIdExist: true });
        } else { resolve(null); }
      });
    });
  }

  static shouldBeUniquePlanId(control: AbstractControl): Promise<ValidationErrors | null> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value as string === '00000') {
          resolve({ shouldBeUniquePlanId: true });
        } else { resolve(null); }
      }, 2000);
    });
  }

  static shouldBeUniqueReportId(control: AbstractControl): Promise<ValidationErrors | null> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value as string === '00000') {
          resolve({ shouldBeUniqueReportId: true });
        } else { resolve(null); }
      }, 2000);
    });
  }
}
