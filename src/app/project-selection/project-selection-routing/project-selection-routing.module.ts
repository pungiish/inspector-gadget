import { NgModule } from '@angular/core';
import { MainComponent } from '../components/main/main.component';
import { Routes, RouterModule } from '@angular/router';
import { NewProjectComponent } from '../components/new-project/new-project.component';
import { OpenProjectComponent } from '../components/open-project/open-project.component';


const projectSelectionRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [{
      path: '',
      redirectTo: 'new-project'
    }, {
      path: 'new-project',
      component: NewProjectComponent
    }, {
      path: 'open-project',
      component: OpenProjectComponent
    }]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(projectSelectionRoutes)],
  exports: [RouterModule]
})
export class ProjectSelectionRoutingModule { }
