import { Component, Input, HostListener, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

type CallbackFunctionVariadic = (...args: any[]) => void;


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true
    }
  ]
})
export class FileUploadComponent implements ControlValueAccessor {

  @Input() fileType: string;

  public file: File | null = null;

  onChange: CallbackFunctionVariadic = (...args: any[]) => { };

  @HostListener('change', ['$event.target.files']) async emitFiles(event: FileList) {
    const file = event && event.item(0);
    this.onChange(file);
    this.file = file;
  }
  constructor(private host: ElementRef<HTMLInputElement>) { }

  writeValue(value: null) {
    // clear file input
    this.host.nativeElement.value = '';
    this.file = null;
  }

  registerOnChange(fn: CallbackFunctionVariadic) {
    this.onChange = fn;
  }

  registerOnTouched(fn: CallbackFunctionVariadic) {
  }


}
