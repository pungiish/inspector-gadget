import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenProjectComponent } from './open-project.component';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('OpenProjectComponent', () => {
  let component: OpenProjectComponent;
  let fixture: ComponentFixture<OpenProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenProjectComponent ],
      /** Providers because it's injected in the contructor */
      providers: [ FormBuilder ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
