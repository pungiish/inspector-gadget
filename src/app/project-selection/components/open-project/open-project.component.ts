import { Component } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import { Router } from '@angular/router';
import { IdValidators } from '../../id.validators';

@Component({
  selector: 'app-open-project',
  templateUrl: './open-project.component.html',
  styleUrls: ['./open-project.component.scss']
})
export class OpenProjectComponent {
  progress = 0;
  uploadResponse: any = { status: '', message: '' };
  error: string;


  openProjectForm = this.fb.group({
    planId: ['', [
        Validators.required,
        Validators.minLength(5)
        ],
      IdValidators.IsPlanIdExist
    ],
    file: [null, Validators.required],
    dxfFile: [null, Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private projectDataService: ApiProjectDataService,
    private router: Router) { }

  onSubmit() {
    if (this.openProjectForm.invalid) {
      return;
    }
    const formData: FormData = this.appendFormData();
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.projectDataService.elements = JSON.parse(fileReader.result as string);
    };
    fileReader.readAsText(this.openProjectForm.value.file);
    // this.projectDataService.readGeoJson(this.openProjectForm.value.file).subscribe(x => console.log(x));
    this.projectDataService.toGeoJson(formData).subscribe(
      (res) => {
        this.uploadResponse = res;
        if (this.uploadResponse.status === 'COMPLETED') {
          this.projectDataService.isLoading$.next(true);
          const element = this.uploadResponse.message.features;
          for (let i = 0; i < element.length; i++) {
            const obj = element[i].geometry?.geometries ?
              element[i].geometry.geometries : element[i].geometry;
            if (!obj) {
              delete element[i];
              continue;
            }
            for (const key in obj) {
              if (Array.isArray(obj)) {
              } else {
                if (obj.coordinates === null) {
                  delete obj[key];
                }
              }
            }
            if (!(Object.keys(obj).length > 0)) {
              element[i].geometry = null;
            }
          }
          this.projectDataService.geoJSON = this.uploadResponse.message;
          this.projectDataService.transformedLayers = this.uploadResponse.message.features.groupBy('properties', 'Layer');
          this.router.navigate(['/project/planning']);
        }
      },
      (err) => this.error = err
    );
  }

  appendFormData(): FormData {
    const formData = new FormData();
    formData.append('upload', this.openProjectForm.value.dxfFile, this.openProjectForm.value.dxfFile.name);
    // formData.append('sourceSrs', 'EPSG:2278');
    // formData.append('targetSrs', 'EPSG:4326');
    return formData;
  }

  get planId() {
    return this.openProjectForm.get('planId');
  }
}
