import { DomEvent, Marker, divIcon, Point } from 'leaflet';
import { Component, ViewChild } from '@angular/core';
import { IdValidators } from '../../id.validators';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import '../../../shared/helpers/arrayExtensions';
import { CsvPointRecordService } from 'shared/services/csvPointRecord.service';
import { NgxCsvParser } from 'ngx-csv-parser';
import { NgxCSVParserError } from 'ngx-csv-parser';
import proj4 from 'proj4';
import L from 'leaflet';


@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss'],
})
export class NewProjectComponent {
  progress = 0;
  uploadResponse: any = { status: '', message: '' };
  error: string;

  newProjectForm = this.fb.group({
    planId: ['', [
      Validators.required,
      Validators.minLength(5)],
      IdValidators.shouldBeUniquePlanId],
    planName: ['', Validators.required],
    reportId: ['', [
      Validators.required,
      Validators.minLength(5)],
      IdValidators.shouldBeUniqueReportId],
    dxfFile: [null, Validators.required],
    csvFile: [''],
  });

  constructor(
    private fb: FormBuilder,
    private projectDataService: ApiProjectDataService,
    private router: Router,
    public csvPointService: CsvPointRecordService,
    private ngxCsvParser: NgxCsvParser
  ) {
  }

  onSubmit() {
    if (this.newProjectForm.invalid) {
      return;
    }
    const formData: FormData = this.appendFormData();
    this.projectDataService.projectForm = this.newProjectForm.value;
    const textCoordinates = this.csvPointService.featureCollection;
    proj4.defs('EPSG:4326');
    this.projectDataService.toGeoJson(formData).subscribe(
      (res) => {
        this.uploadResponse = res;
        this.projectDataService.isLoading$.next(true);
        if (this.uploadResponse.status === 'COMPLETED') {

          /**
                * Remove null coordinates from geoJSON. Else the toGeoJSON function returns an error.
                * If the coordinates of the geometry object are null, remove the properties in the geometry object
                * then remove the whole feature object.
           */
          /*  this.uploadResponse.message.crs.properties = {
             name: 'EPSG:4326'
           } */
          this.uploadResponse.message.features.push(textCoordinates)
          const element = this.uploadResponse.message.features;
          for (let i = 0; i < element.length; i++) {
            /*  element[i].crs = {
               type: "name",
               properties: {
                 name: "EPSG:4326"
               }
             } */
            const obj = element[i].geometry?.geometries ?
              element[i].geometry.geometries : element[i].geometry;
            if (!obj) {
              delete element[i];
              continue;
            }
            for (const key in obj) {
              if (Array.isArray(obj)) {
              } else {
                if (obj.coordinates === null) {
                  console.log(obj[key]);
                  delete obj[key];
                }
              }
            }
            if (!(Object.keys(obj).length > 0)) {
              element[i].geometry = null;
            }
          }
          this.projectDataService.geoJSON = this.uploadResponse.message;
          this.projectDataService.transformedLayers = this.uploadResponse.message.features.groupBy('properties', 'Layer');
          this.router.navigate(['/project/planning']);
        }
      },
      (err) => this.error = err
    );
  }

  handleCSV(csvData) {
    const files = csvData.srcElement.files;
    this.ngxCsvParser.parse(files[0], { header: true, delimiter: ';' })
      .pipe().subscribe((result: Array<any>) => {
        this.csvPointService.uploadListener(result);
      }, (error: NgxCSVParserError) => {
        console.error('Error', error);
      });

  }

  appendFormData(): FormData {
    const formData = new FormData();
    formData.append('upload', this.dxfFile.value, this.dxfFile.value.name);
    // formData.append('sourceSrs', 'EPSG:2278');
    // formData.append('targetSrs', 'EPSG:4326');
    return formData;
  }

  get planId() {
    return this.newProjectForm.get('planId');
  }
  get planName() {
    return this.newProjectForm.get('planName');
  }
  get reportId() {
    return this.newProjectForm.get('reportId');
  }
  
  get dxfFile() {
    return this.newProjectForm.get('dxfFile');
  }
}
