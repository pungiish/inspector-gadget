import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

/** MATERIAL */
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';


/** COMPONENTS */
import { MainComponent } from './components/main/main.component';
import { NewProjectComponent } from './components/new-project/new-project.component';
import { OpenProjectComponent } from './components/open-project/open-project.component';

/** ROUTING */
import { ProjectSelectionRoutingModule } from './project-selection-routing/project-selection-routing.module';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { SharedModule } from 'shared/shared.module';

import { NgxCsvParserModule } from 'ngx-csv-parser';

const MATERIAL_MODULES = [
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressBarModule,
];

@NgModule({
  declarations: [MainComponent, NewProjectComponent, OpenProjectComponent, FileUploadComponent],
  imports: [
    CommonModule,
    ProjectSelectionRoutingModule,
    ...MATERIAL_MODULES,
    ReactiveFormsModule,
    SharedModule,
    NgxCsvParserModule
  ]
})
export class ProjectSelectionModule { }
