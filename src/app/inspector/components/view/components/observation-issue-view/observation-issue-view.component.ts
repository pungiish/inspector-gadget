import { Component, OnInit, Input, EventEmitter, Output, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { Issue, Verification } from 'shared/models/models';
import { FormBuilder } from '@angular/forms';
import { StoredIssuesService } from 'inspector/services/stored-issues-service/stored-issues.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-observation-issue-view',
  templateUrl: './observation-issue-view.component.html',
  styleUrls: ['./observation-issue-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush  
})
export class ObservationIssueViewComponent implements OnInit, OnChanges {

  @Input() issue: Issue;
  @Output() toggleEditMode = new EventEmitter();
  @Output() onDelete = new EventEmitter<Issue>();
  @Output() resize = new EventEmitter();
  @Output() accept = new EventEmitter<Issue>();

  unit: Observable<string>;

  issueForm = this.fb.group({
    discipline: [{ value: '', disabled: true }],
    subdiscipline: [{ value: '', disabled: true }],
    classification: [{ value: '', disabled: true }],
    subclassification: [{ value: '', disabled: true }],
    comment: [{ value: '', disabled: true }],
    measurement: [{ value: '', disabled: true }]
  });


  constructor(private fb: FormBuilder, private storedIssuesService: StoredIssuesService) {
    this.unit = storedIssuesService.measurementObs$;
  }

  ngOnInit(): void {
    console.log('obsservation issue view');
    this.issueForm.controls.discipline.setValue(this.issue.discipline);
    this.issueForm.controls.subdiscipline.setValue(this.issue.subdiscipline);
    this.issueForm.controls.classification.setValue((this.issue.verification as Verification).classification);
    this.issueForm.controls.subclassification.setValue((this.issue.verification as Verification).subclassification);
    this.issueForm.controls.comment.setValue((this.issue.verification as Verification).comment);
    this.issueForm.controls.measurement.setValue((this.issue.verification as Verification).measurement);

  }

  ngOnChanges() {
    console.log(this.issue);
    this.issueForm.controls.discipline.setValue(this.issue.discipline);
    this.issueForm.controls.subdiscipline.setValue(this.issue.subdiscipline);
    this.issueForm.controls.classification.setValue((this.issue.verification as Verification).classification);
    this.issueForm.controls.subclassification.setValue((this.issue.verification as Verification).subclassification);
    this.issueForm.controls.comment.setValue((this.issue.verification as Verification).comment);
    this.issueForm.controls.measurement.setValue((this.issue.verification as Verification).measurement);
  }

  onEdit() {
    this.toggleEditMode.emit();
  }

  onRemove() {
    this.onDelete.emit(this.issue);
  }

  onResize() {
    this.resize.emit();
  }

  onAccept() {
    this.accept.emit(this.issue);
  }


}
