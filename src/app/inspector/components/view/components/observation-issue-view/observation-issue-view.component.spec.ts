import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservationIssueViewComponent } from './observation-issue-view.component';

describe('ObservationIssueViewComponent', () => {
  let component: ObservationIssueViewComponent;
  let fixture: ComponentFixture<ObservationIssueViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservationIssueViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservationIssueViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
