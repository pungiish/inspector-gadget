import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Issue, Verification } from 'shared/models/models';

@Component({
  selector: 'app-issue-view',
  templateUrl: './issue-view.component.html',
  styleUrls: ['./issue-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueViewComponent implements OnInit {

  @Input() issue: Issue;

  @Output() onDelete = new EventEmitter<Issue>();
  @Output() accept = new EventEmitter<Issue>();
  @Output() toggleEditMode = new EventEmitter<Issue>();
  @Output() resize = new EventEmitter();
  @Output() updateElement = new EventEmitter<Issue>();


  constructor() { }

  ngOnInit(): void {
  }

  onEdit() {
    this.toggleEditMode.emit(this.issue);
  }

  onRemove() {
    this.onDelete.emit(this.issue);
  }

  onAccept() {
    this.accept.emit(this.issue);
  }

  onResize() {
    this.resize.emit();
  }

  setVerification(verification: string | Verification) {
    if (verification === 'observation') {
      let setVerification = {
        classification: undefined,
        subclassification: undefined,
        comment: undefined,
        measurement: undefined
      };
      this.updateElement.emit({
        _id: this.issue._id,
        discipline: this.issue.discipline,
        subdiscipline: this.issue.subdiscipline,
        verification: setVerification,
        coordinates: this.issue.coordinates,
        accept: false
      });
      this.onEdit();
    } else {
      this.updateElement.emit({
        _id: this.issue._id,
        discipline: this.issue.discipline,
        subdiscipline: this.issue.subdiscipline,
        verification: verification,
        coordinates: this.issue.coordinates,
        accept: false
      });
    }
  }

}
