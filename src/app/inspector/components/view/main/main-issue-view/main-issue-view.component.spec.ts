import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainIssueViewComponent } from './main-issue-view.component';

describe('MainIssueViewComponent', () => {
  let component: MainIssueViewComponent;
  let fixture: ComponentFixture<MainIssueViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainIssueViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainIssueViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
