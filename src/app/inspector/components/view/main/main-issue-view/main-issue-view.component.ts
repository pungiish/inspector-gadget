import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { StoredIssuesService } from 'inspector/services/stored-issues-service/stored-issues.service';
import { Issue, Verification } from 'shared/models/models';

@Component({
  selector: 'app-main-issue-view',
  templateUrl: './main-issue-view.component.html',
  styleUrls: ['./main-issue-view.component.scss'],
})
export class MainIssueViewComponent implements OnInit {
  isEditMode$: Observable<boolean>;
  selectedIssue$: Observable<Issue>;

  @Output() createElement = new EventEmitter<Issue>();
  @Output() updateElement = new EventEmitter<Issue>();
  @Output() acceptElement = new EventEmitter<Issue>();
  @Output() deleteElement = new EventEmitter<Issue>();
  @Output() resize = new EventEmitter();
  @Output() openMinimap = new EventEmitter();


  constructor(private storedIssues: StoredIssuesService) {
    this.isEditMode$ = this.storedIssues.editModeObs$;
    this.selectedIssue$ = this.storedIssues.selectedIssueObs$;
  }

  ngOnInit(): void {
  }

  onElementUpdated(issue: Issue) {
    console.log(issue);
    this.updateElement.emit(issue);
    if (!!issue.verification && issue.verification !== 'observation' || issue.accept) {
      this.storedIssues.removeEditMode();

    }
  }

  onElementAccepted(issue: Issue) {;
    this.acceptElement.emit(issue);
  }

  onElementCreated(issue: Issue) {
    this.createElement.emit(issue);
    this.storedIssues.removeEditMode();

  }

  onElementDeleted(issue: Issue) {
    this.deleteElement.emit(issue);
  }

  toggleEditMode() {
    this.storedIssues.addEditMode();
  }

  onResize() {
    this.resize.emit();
  }

  onOpenMinimap() {
    this.openMinimap.emit();
  }

}
