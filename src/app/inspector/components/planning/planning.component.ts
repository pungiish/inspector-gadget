import 'leaflet-rotatedmarker';
import { Component, NgZone, AfterViewInit, ElementRef, Renderer2, ViewChild, OnDestroy, ViewEncapsulation } from '@angular/core';
import { latLng, CircleMarker, Map, FeatureGroup, latLngBounds, Layer, Icon, control, Control, Marker, LeafletKeyboardEvent, polyline, divIcon, Point, Polyline, LatLng, DivIcon, geoJSON } from 'leaflet';
import '../../../shared/helpers/arrayExtensions';
import { CSS_COLOR_NAMES } from '../../helpers/colours';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import * as L from 'leaflet';
import 'leaflet-geometryutil';
import '../../../../../node_modules/leaflet-minimap/src/Control.MiniMap.js';
import '@geoman-io/leaflet-geoman-free';
import '../../../../assets/leaflet.groupedlayercontrol.min.js';
import { IGeoJson, Issue, Feature } from 'shared/models/models';
import { saveAs } from 'file-saver';
import { StoredIssuesService } from 'inspector/services/stored-issues-service/stored-issues.service';
import { Subscription } from 'rxjs';
import 'leaflet-simple-map-screenshoter';
import { PluginOptions } from 'leaflet-simple-map-screenshoter';
import * as JSZip from 'jszip';
import center from '@turf/center';
import { points } from '@turf/helpers';
import { MatSnackBar } from '@angular/material/snack-bar';
import 'proj4leaflet';
import proj4 from "proj4";
import 'leaflet-polylinedecorator';
import 'leaflet-draw'

/**
 * TODO: Check for issues with latlong parsing ( they parse in LatLong (YX) coordinates, but store and work with LongLat coordinates.. ) 
 * npm uninstall leaflet-mouse-position (used for testing).
 */
import { Proj4GeoJSONFeature } from 'proj4leaflet';
import '../../../shared/pluginExtensions/divIcon';
import { Projection } from '../../../shared/pluginExtensions/leaflet-projections';
import { LeafletService } from 'inspector/services/leaflet-service/leaflet.service';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlanningComponent implements AfterViewInit, OnDestroy {
  @ViewChild('minimap1') minimapDOM;
  // Options for the map
  options = {
    zoom: 1,
    maxZoom: 10,
    minZoom: -10,
    scrollWheelZoom: center,
    rotate: true,
    center: latLng(0, 0),
    crs: L.CRS.Simple,
  };

  defaultIcon = new Icon({
    iconUrl: 'assets/icons/selectedPoint.svg',
    iconSize: [30, 30]
  });

  undefinedIcon = new Icon({
    iconUrl: 'assets/icons/clickedIssue.png',
    iconSize: [40, 40],
  });

  obsIcon = new Icon({
    iconUrl: 'assets/icons/selected issue.svg',
    iconSize: [40, 40],
  });

  // Screenshot blob
  blobs = [];
  screenshot = false;


  screenshotPluginOptions: PluginOptions = {
    cropImageByInnerWH: false, // crop blank opacity from image borders
    hidden: true, // hide screen icon
    domtoimageOptions: {}, // see options for dom-to-image
    position: 'topleft', // position of take screen icon
    screenName: 'screen', // string or function
    iconUrl: '', // screen btn icon base64 or url
    hideElementsWithSelectors: ['.leaflet-control-container'], // by default hide map controls everything else must be child of _map._container
    mimeType: 'image/png', // used if format == image,
    caption: null, // streeng or function, added caption to bottom of screen
    captionFontSize: 15,
    captionFont: 'Arial',
    captionColor: 'black',
    captionBgColor: 'white',
    captionOffset: 5,
  };
  format = 'blob'; // 'image' - return base64, 'canvas' - return canvas
  overridedScreenshotPluginOptions = {
    mimeType: 'image/jpeg'
  };

  simpleMapScreenshoter;


  transformedLayers: Array<any>;
  map: Map;
  minimaplayersControl: any = { baseLayers: {}, overlays: {} };
  minimap: Control;
  showMinimap = true;

  resize = false;
  toggledSidebar = false;
  loadingMinimap = false;
  elementToggle = false;

  measuredDistance = "";
  isEdit: boolean;
  isEditSub: Subscription;

  snackbarDuration = 700;

  constructor(
    private zone: NgZone,
    private elem: ElementRef,
    private renderer: Renderer2,
    private storedIssuesService: StoredIssuesService,
    private snackbar: MatSnackBar,
    private leafletService: LeafletService,
    public projectDataService: ApiProjectDataService,
  ) {
    // proj4.defs['OGC:CRS84'] = proj4.defs['EPSG:4326'];
    this.isEditSub = this.storedIssuesService.editModeObs$.subscribe(isEdit => {
      this.isEdit = isEdit;
      if (this.isEdit) {
        if (this.resize) {
          this.resize = false;
          if (!this.showMinimap) {
            this.showMinimap = true;
            this.toggleMinimap();
          }
        }
      }
    });
    this.leafletService.groupedLayers2Geojson(this.projectDataService.transformedLayers);
  }



  ngAfterViewInit() {
    /** MOVING THE MINIMAP TO POSITION. */
    const newParent1: HTMLElement = document.getElementById('minimap1') as HTMLElement;
    const oldParent1 = this.map._controlContainer.lastChild;
    while (oldParent1.childNodes.length > 0) {
      newParent1.appendChild(oldParent1.childNodes[0]);
    }
  }

  onMapReady(event: Map) {
    this.map = event;
    this.leafletService.map = event;

    setTimeout(() => {
      this.projectDataService.isLoading$.next(false);
    }, 0);
    // @ts-ignore
    this.leafletService.setLayerWithAxes()

    // Loaded Issues from file
    this.leafletService.setDrawnIssues(this.projectDataService.elements);

    this.leafletService.setDrawnIssuesClickListener();

    // TODO: convertTextCoordinates

    this.map.on('pm:create', (created: any) => {
      this.zone.run(() => {
        this.map.pm.disableDraw('Marker');
        this.leafletService.layerWithIssue = created.layer;
        let newIssue: Issue = {
          _id: undefined,
          discipline: undefined,
          subdiscipline: undefined,
          verification: undefined,
          coordinates: created.marker._latlng,
          accept: false
        };
        this.storedIssuesService.selectIssue(newIssue);
        this.storedIssuesService.addEditMode();
      });
    });

    this.map.on('pm:remove', e => {
      const id = e.layer.feature.properties.issue._id;
      this.storedIssuesService.removeIssue(id);
      this.leafletService.removeIssue(e.layer);
    });
    this.map.on('zoomend', () => {
      console.log(this.map.getZoom());
      let zoom = this.map.getZoom();
      switch (zoom) {
        case -10: case -9: case -8: case -7: case -6: case -5:
          this.map.eachLayer((layer) => {
            if (layer instanceof L.Marker && layer.feature && layer.feature.properties && layer.feature.properties.Text) {
              this.zone.run(() => {
                layer.setIcon(divIcon({
                  iconSize: new L.Point(0, 0),
                  html: layer.feature.properties.Text,
                  className: 'text-background',
                }))
              })
            }
          })
          break;
        case -4: case -3: case -2: case -1:
          this.map.eachLayer((layer) => {
            if (layer instanceof L.Marker && layer.feature && layer.feature.properties && layer.feature.properties.Text) {
              this.zone.run(() => {
                layer.setIcon(divIcon({
                  iconSize: new L.Point(0, 0),
                  html: layer.feature.properties.Text,
                  className: 'text-background-1',
                }))
              })
            }
          })
          break;
        case 0: case 1:
          this.map.eachLayer((layer) => {
            if (layer instanceof L.Marker && layer.feature && layer.feature.properties && layer.feature.properties.Text) {
              this.zone.run(() => {
                layer.setIcon(divIcon({
                  iconSize: new L.Point(0, 0),
                  html: layer.feature.properties.Text,
                  className: 'text-background-2',
                }))
              })
            }
          })
          break;
        default:
          this.map.eachLayer((layer) => {
            if (layer instanceof L.Marker && layer.feature && layer.feature.properties && layer.feature.properties.Text) {
              this.zone.run(() => {
                layer.setIcon(divIcon({
                  iconSize: new L.Point(0, 0),
                  html: layer.feature.properties.Text,
                  className: 'text-background-3',
                }))
              })
            }
          })
          break;
      }
      if (zoom < 10) {
      }
    })
    this.initializeMap();
  }

  initializeMap() {
    this.leafletService.initializeMap(this.snackbar);
    this.simpleMapScreenshoter = L.simpleMapScreenshoter(this.screenshotPluginOptions).addTo(this.map);
  }

  
  rotate() {
    this.map.setBearing(this.map.getBearing() + 45);
  }


  onElementCreated(issue: Issue) {
    this.storedIssuesService.saveIssue(issue);
    const layer = this.leafletService.layerWithIssue;
    const feature = (layer as any).feature = (layer as any).feature || {}; // Initialize feature
    feature.type = feature.type || 'Feature'; // Initialize feature.type
    let props = feature.properties = feature.properties || {}; // Initialize feature.properties
    props.issue = issue;
    this.leafletService.drawnElements.addLayer(layer);
    const elements = this.elem.nativeElement.querySelectorAll('.button-container');
    elements.forEach(element => {
      this.renderer.setStyle(element, 'visibility', 'visible');
    });
  }

  onElementUpdated(event) {
    this.storedIssuesService.editIssue({
      _id: event._id,
      discipline: event.discipline,
      subdiscipline: event.subdiscipline,
      verification: event.verification,
      coordinates: event.coordinates,
      accept: event.accept
    });

    this.leafletService.drawnElements.eachLayer(layer => {
      if (layer.feature.properties.issue._id === event._id) {
        layer.feature.properties.issue = event;
      }
    });
  }

  onElementDeleted(issue: Issue) {
    this.storedIssuesService.removeIssue(issue._id);
    let layerToDelete = this.leafletService.drawnElements.getLayers().find(layer => {
      return layer.feature.properties.issue._id === issue._id;
    });
    this.leafletService.removeIssue(layerToDelete);
  }

  onElementAccepted(issue: Issue) {
    let updatedIssue = this.storedIssuesService.editIssue({ ...issue, accept: true })
    this.leafletService.drawnElements.eachLayer(layer => {
      if (layer.feature.properties.issue._id === issue._id) {
        layer.feature.properties.issue = updatedIssue;
      }
    });
  }

  

  onResize() {
    this.showMinimap = false;
    this.resize = !this.resize;
    setTimeout(() => {
      this.map.invalidateSize();
    }, 0);
    if (this.storedIssuesService.editMode$.value === true && this.resize && this.showMinimap) {
      this.showMinimap = true;
    }
    if (!this.resize && !this.toggledSidebar) {
      //this.showMinimap = true;
      //this.toggleMinimap();
    }
  }

  zoomOut() {
    this.map.setZoom(this.map.getZoom() - 2);
  }

  onOpenMinimap() {
    if (!this.showMinimap) {
      this.showMinimap = true;
      this.toggleMinimap();
    }
  }

  saveBlobs() {
    let zip = new JSZip();
    let counter = 0;
    this.blobs.forEach((blob) => {
      zip.file(`issue(${counter}).png`, blob);
      counter += 1;
    });
    zip.generateAsync({ type: 'blob' }).then(function (blob) {
      saveAs(blob, "Issues.zip");
    }, function (err) {
      console.error(err);
    });
    this.projectDataService.isLoading$.next(false);
    this.blobs = [];
  }

  toggleMinimap() {
    this.loadingMinimap = true;
    if (this.showMinimap) {
      setTimeout(() => {
        this.map.invalidateSize();
        this.leafletService.minimap.addTo(this.map);
        /** MOVING THE MINIMAP TO POSITION. */
        const newParent1: HTMLElement = document.getElementById('minimap1') as HTMLElement;
        const oldParent1 = this.map._controlContainer.lastChild;
        while (oldParent1.childNodes.length > 0) {
          newParent1.appendChild(oldParent1.childNodes[0]);
        }
        this.loadingMinimap = false;
      }, 500);
    }
  }


  toggleElements() {
    let thisIcon;
    this.elementToggle = !this.elementToggle;
    this.leafletService.drawnElements.eachLayer((layer: Layer) => {
      if (this.elementToggle) {
        if ((layer as any).feature.properties.issue.verification === 'verified') {
          thisIcon = new Icon({
            iconUrl: 'assets/icons/selectedPoint.svg',
            iconSize: [40, 40],
          });
        } else {
          thisIcon = new Icon({
            iconUrl: 'assets/icons/selected issue.svg',
            iconSize: [40, 40],
          });
        }
        (layer as Marker).setIcon(thisIcon);
      } else {
        (layer as Marker).setIcon(this.defaultIcon);
      }
    });
  }

  saveElements() {
    const drawnGeoJSON = this.leafletService.drawnElements.toGeoJSON();
    drawnGeoJSON['planId'] = this.projectDataService.projectForm.planId || 0;
    drawnGeoJSON['planName'] = this.projectDataService.projectForm.planName || 'test';
    const jsonData = JSON.stringify(drawnGeoJSON);
    const file = new File([jsonData], this.projectDataService.projectForm.planName + '.json',
      { type: 'data:application/json;charset=utf-8' });
    saveAs(file);
  }

  clearScreen() {
    this.toggledSidebar = false;
    this.resize = true;
    setTimeout(() => {
      this.map.invalidateSize();
    }, 0);
    this.projectDataService.isLoading$.next(true);
  }

  ScreenshotIssues(idx: number) {
    if (this.toggledSidebar) {
      this.clearScreen();
    }
    let issueIdx = idx;
    this.map.once('moveend', () => {
      setTimeout(() => {
        this.map.invalidateSize();
        this.simpleMapScreenshoter.takeScreen(this.format, this.overridedScreenshotPluginOptions).then(blob => {
          this.blobs.push(blob);
          issueIdx += 1;
          if (this.blobs.length === this.storedIssuesService.issues$.value.length) {
            this.screenshot = false;
            this.saveBlobs();
          } else {
            this.ScreenshotIssues(issueIdx);
          }
        }).catch(e => {
          console.error(e);
        });
      }, 0);
    });
    this.map.setView(this.storedIssuesService.issues$.value[issueIdx].coordinates, 4, { animate: false });
  }

  ngOnDestroy() {
    if (!!this.isEditSub) {
      this.isEditSub.unsubscribe();
    }
    this.storedIssuesService.resetData();
    this.projectDataService.resetData();
  }
}
