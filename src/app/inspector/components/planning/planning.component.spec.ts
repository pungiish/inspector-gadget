import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { PlanningComponent } from './planning.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import { SharedModule } from 'shared/shared.module';
import { of } from 'rxjs';

class MockGeoJsonVt {
  options: {};
  tiles: [{
    features: [{
      geometry: [963, 1693];
      type: 1;
      tags: {
        Layer: '0';
        SubClasses: 'AcDbEntity:AcDbMText';
        ExtendedEntity: null;
        Linetype: null;
        EntityHandle: '20000';
        Text: '';
      };
    }];
  }];
  tileCoords: [{ z: 0, x: 0, y: 0; }];

  getTile() {
    return {
      features: [{
        geometry: [963, 1693],
        type: 1,
        tags: {
          Layer: '0',
          SubClasses: 'AcDbEntity:AcDbMText',
          ExtendedEntity: null,
          Linetype: null,
          EntityHandle: '20000',
          Text: ''
        }
      }]
    };
  }
}

class FakeApiService {
  sharedLayers = {
    type: 'FeatureCollection',
    crs: { type: 'name' },
    features: [{
      type: 'Feature',
      properties: {
        Layer: 'layerName'
      }
    }]
  };
  // Implement the methods you want to overload here
  getData() {
    return of({ features: {} }); // * mocks the return of the real method
  }
}

describe('PlanningComponent', () => {
  let component: PlanningComponent;
  let fixture: ComponentFixture<PlanningComponent>;
  let tileIndex: MockGeoJsonVt;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [PlanningComponent],
      imports: [HttpClientTestingModule, SharedModule],
      providers: [{
        provide: ApiProjectDataService,
        useClass: FakeApiService
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningComponent);
    component = fixture.componentInstance;
    tileIndex = new MockGeoJsonVt();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
