import { Component, OnInit } from '@angular/core';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  constructor(
    private projectDataService: ApiProjectDataService,
    private router: Router) {
  }
}
