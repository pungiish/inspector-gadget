import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Issue, Verification } from 'shared/models/models';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import { FormBuilder, Validators } from '@angular/forms';
import { StoredIssuesService } from 'inspector/services/stored-issues-service/stored-issues.service';

@Component({
  selector: 'app-observation-issue-edit',
  templateUrl: './observation-issue-edit.component.html',
  styleUrls: ['./observation-issue-edit.component.scss']
})
export class ObservationIssueEditComponent implements OnInit, OnChanges {

  @Input() issue: Issue;

  @Output() updateElement = new EventEmitter<Issue>();
  @Output() createElement = new EventEmitter<Issue>();

  verificationForm = this.fb.group({
    discipline: ['', Validators.required],
    subdiscipline: ['', Validators.required],
    classification: ['', Validators.required],
    subclassification: [''],
    comment: [''],
    measurement: ['']
  });

  measurementForm = this.fb.group({
    measurement: [this.storedIssueService.measurementUnit$.value]
  });

  firstCreation = false;
  data = [];
  disciplines = [];
  classifications = [];
  subclassifications = [];
  constructor(private projectDataService: ApiProjectDataService,
    private storedIssueService: StoredIssuesService,
    private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.data = this.projectDataService.dataArr;
    this.data.forEach(element => {
      this.disciplines.push(element.trade_name);
    });
    this.classifications = this.projectDataService.dataArr.find(x => x.trade_name === this.issue.discipline).subdisciplines.find(x => x.subdiscipline_name === this.issue.subdiscipline).classifications;
    this.subclassifications = this.projectDataService.dataArr.find(x => x.trade_name === this.issue.discipline).subdisciplines.find(x => x.subdiscipline_name === this.issue.subdiscipline).subclassifications;
    if (this.issue.verification === 'observation') {
      this.firstCreation = true;
      this.verificationForm.controls.discipline.setValue(this.issue.discipline);
      this.verificationForm.controls.subdiscipline.setValue(this.issue.subdiscipline);
    } else {
      this.verificationForm.controls.discipline.setValue(this.issue.discipline);
      this.verificationForm.controls.subdiscipline.setValue(this.issue.subdiscipline);
      this.verificationForm.controls.classification.setValue((this.issue.verification as Verification).classification);
      this.verificationForm.controls.subclassification.setValue((this.issue.verification as Verification).subclassification);
      this.verificationForm.controls.comment.setValue((this.issue.verification as Verification).comment);
      this.verificationForm.controls.measurement.setValue((this.issue.verification as Verification).measurement);
    }

    this.measurementForm
      .controls['measurement']
      .valueChanges
      .subscribe(selectedValue => {
        this.storedIssueService.measurementUnit$.next(selectedValue);
        if (!!this.verificationForm.controls.measurement.value) {
          if (selectedValue === 'inches') {
            this.verificationForm.controls.measurement.setValue(this.verificationForm.controls.measurement.value * 39.3700787);
          } else {
            this.verificationForm.controls.measurement.setValue(this.verificationForm.controls.measurement.value * 0.0254);
          }
        }
        /* this.storedIssueService.issues$.value.forEach(issue => {
          if (selectedValue === 'inches') {
            (issue.verification as Verification).measurement = (issue.verification as Verification).measurement * 39.3700787;
          } else {
            (issue.verification as Verification).measurement = (issue.verification as Verification).measurement * 0.0254;
          }
        }); */

      });
  }

  getSubdiscipline() {
    return (this.data.find(x => x.trade_name === this.verificationForm.controls.discipline.value).subdisciplines);
  }

  onSubmit() {
    this.updateElement.emit({
      _id: this.issue._id,
      discipline: this.verificationForm.controls.discipline.value,
      subdiscipline: this.verificationForm.controls.subdiscipline.value,
      verification: this.verificationForm.value,
      coordinates: this.issue.coordinates,
      accept: this.issue.accept
    });

  }
  ngOnChanges() {
    if ((this.issue.verification as Verification).hasOwnProperty('measurement')) {
      this.verificationForm.controls.measurement.setValue((this.issue.verification as Verification).measurement);
    }
  }

}
