import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservationIssueEditComponent } from './observation-issue-edit.component';

describe('ObservationIssueEditComponent', () => {
  let component: ObservationIssueEditComponent;
  let fixture: ComponentFixture<ObservationIssueEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservationIssueEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservationIssueEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
