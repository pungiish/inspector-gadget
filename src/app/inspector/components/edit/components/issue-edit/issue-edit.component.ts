import { Component, OnInit, Input, OnChanges, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Issue, Verification } from 'shared/models/models';
import { Validators, FormBuilder } from '@angular/forms';
import { ApiProjectDataService } from 'shared/services/api-data.service';
import { LatLng } from 'leaflet';
import { StoredIssuesService } from 'inspector/services/stored-issues-service/stored-issues.service';

@Component({
  selector: 'app-issue-edit',
  templateUrl: './issue-edit.component.html',
  styleUrls: ['./issue-edit.component.scss']
})
export class IssueEditComponent implements OnInit, OnChanges {

  @Input() issue: Issue;

  @Output() updateElement = new EventEmitter<Issue>();
  @Output() createElement = new EventEmitter<Issue>();
  @Output() toggle = new EventEmitter();


  disciplines: any = [];
  subdisciplines: any = [];
  // classifications: any;
  data: any;

  insertOrUpdate = 'Insert';

  fileIssueForm = this.fb.group({
    discipline: ['', Validators.required],
    subdiscipline: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private projectDataService: ApiProjectDataService,
    private storedIssuesService: StoredIssuesService
  ) {
    this.data = this.projectDataService.dataArr;
    this.data.forEach(element => {
      this.disciplines.push(element.trade_name);
    });
  }

  ngOnInit() {
  }

  getSubdiscipline() {
    return (this.data.find(x => x.trade_name === this.fileIssueForm.controls.discipline.value).subdisciplines);
  }

  onSubmit() {
    if (this.fileIssueForm.valid) {
      if (this.insertOrUpdate === 'Update') {
        this.onUpdate();
        this.fileIssueForm.reset();
        this.fileIssueForm.markAsPristine();
        return;
      }
      this.createElement.emit({
        _id: this.storedIssuesService.issues$.getValue.length,
        discipline: this.fileIssueForm.controls.discipline.value,
        subdiscipline: this.fileIssueForm.controls.subdiscipline.value,
        verification: undefined,
        coordinates: this.issue.coordinates,
        accept: this.issue.accept
      });
    }
    this.fileIssueForm.reset();
    this.fileIssueForm.markAsPristine();
  }
  onUpdate() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'issue': {
            if (!!this.issue && !!this.issue.verification) {
              this.insertOrUpdate = 'Update';
              this.fileIssueForm.controls.discipline.setValue(this.issue.discipline);
              this.fileIssueForm.controls.subdiscipline.setValue(this.issue.subdiscipline);
              // this.fileIssueForm.controls.classification.setValue(this.issue.element.classification);
            } else {
              this.insertOrUpdate = 'Insert';
              this.fileIssueForm.controls.discipline.setValue('Plumbing');
              this.fileIssueForm.controls.subdiscipline.setValue('Risers');
            }
          }
        }
      }
    }
  }

  onToggle() {
    this.toggle.emit();
  }

}
