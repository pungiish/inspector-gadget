import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainIssueEditComponent } from './main-issue-edit.component';

describe('MainIssueEditComponent', () => {
  let component: MainIssueEditComponent;
  let fixture: ComponentFixture<MainIssueEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainIssueEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainIssueEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
