import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Issue } from 'shared/models/models';
import { LatLng } from 'leaflet';

@Component({
  selector: 'app-main-issue-edit',
  templateUrl: './main-issue-edit.component.html',
  styleUrls: ['./main-issue-edit.component.scss'],
})
export class MainIssueEditComponent implements OnInit {
  @Input() issue: Issue;

  @Output() createElement = new EventEmitter();
  @Output() updateElement = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onElementCreated(issue: Issue) {
    this.createElement.emit(issue);
  }

  onElementUpdated(issue: Issue) {
    this.updateElement.emit(issue);
  }

}
