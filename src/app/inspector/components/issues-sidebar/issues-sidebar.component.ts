import { Component, OnInit, Input, OnDestroy, ViewEncapsulation, Output, EventEmitter, NgZone } from '@angular/core';
import { Issue } from 'shared/models/models';
import { StoredIssuesService } from 'inspector/services/stored-issues-service/stored-issues.service';
import { Map } from 'leaflet';
import { groupBy } from '../../helpers/issue';
import { Subscription, Observable } from 'rxjs';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import * as L from 'leaflet';
import { filter, map } from 'rxjs/operators';


@Component({
  selector: 'app-issues-sidebar',
  templateUrl: './issues-sidebar.component.html',
  styleUrls: ['./issues-sidebar.component.scss'],
})
export class IssuesSidebarComponent implements OnDestroy {

  @Input() _map: Map;

  @Output() onSidebarClose = new EventEmitter();
  @Output() onScreenshot = new EventEmitter();


  issues: Issue[];
  accepted: Issue[];
  notAccepted: Issue[];
  pointsTab: any;
  issuesTab: any;
  disciplines: string[] = [];
  verifications: string[] = [];
  sub: Subscription;
  acceptedIssues: Observable<Issue[]>;
  notAcceptedIssues: Observable<Issue[]>;

  issueRename = {
    '': 'Issues without Verification',
    undefined: 'Undefined Issues',
    verified: 'Verified Issues',
    '[object Object]': 'Issues with Observations'
  };

  constructor(public storeIssuesService: StoredIssuesService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
  ) {
    this.matIconRegistry.addSvgIcon(
      'close-sidebar',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/expandedpointlist.svg')
    );
    this.sub = this.storeIssuesService.issuesObs$.subscribe(issueState => {
      console.log(this.issues);
      this.issues = issueState;
      this.pointsTab = groupBy(this.issues, 'discipline');
      this.disciplines = Object.keys(this.pointsTab);
      this.issuesTab = groupBy(this.issues, 'verification');
      this.verifications = Object.keys(this.issuesTab);
    });

    this.acceptedIssues = this.storeIssuesService.issuesObs$.pipe(
      map(issues => issues.filter(issue => !!issue.accept))
    );
    this.notAcceptedIssues = this.storeIssuesService.issuesObs$.pipe(
      map(issues => issues.filter(issue => !issue.accept))
    );
  }

  selectedIssue(issue: Issue) {
    this.storeIssuesService.selectIssue(issue);
    console.log(issue);
    this._map.setView(issue.coordinates, 24);
    this._map.invalidateSize();
  }

  closeSidebar() {
    this.onSidebarClose.emit();
  }

  screenShot() {
    this.onScreenshot.emit();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.sub.unsubscribe();
  }

}
