import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuesSidebarComponent } from './issues-sidebar.component';

describe('IssuesSidebarComponent', () => {
  let component: IssuesSidebarComponent;
  let fixture: ComponentFixture<IssuesSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuesSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
