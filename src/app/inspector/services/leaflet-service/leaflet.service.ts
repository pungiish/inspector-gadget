import { Injectable, NgZone } from '@angular/core';
import { Feature, IFeatureCollection, IGeoJson, Issue } from 'shared/models/models';
import { geoJSON, FeatureGroup, CircleMarker, LatLng, Layer, latLng, Marker, Map, Point, Icon, DivIcon, polyline, LeafletKeyboardEvent, divIcon, Polyline, latLngBounds, Control, point, control } from 'leaflet';
import { CSS_COLOR_NAMES } from '../../helpers/colours';
import { Proj4GeoJSONFeature } from 'proj4leaflet';
import center from '@turf/center'
import { StoredIssuesService } from '../stored-issues-service/stored-issues.service';
import '../../../shared/pluginExtensions/divIcon';
import { points } from '@turf/helpers';
import { Projection } from 'shared/pluginExtensions/leaflet-projections';
import L from 'leaflet';
import '@geoman-io/leaflet-geoman-free';
import { MatSnackBar } from '@angular/material/snack-bar';



interface LayersControl {
  baseLayers: {} | FeatureGroup<{}>,
  groupedLayers?: {} | FeatureGroup<{}>,
  overlays?: {} | FeatureGroup<{}>
}


@Injectable({
  providedIn: 'root'
})
export class LeafletService {
  metersToInches = 39.3700787;

  map: Map;
  placeOnCenter = false;
  layersFeatureGroup: FeatureGroup;
  minimapFeatureGroup: FeatureGroup;

  // Layer to be referenced when Issue is created
  layerWithIssue: Layer;
  transformedLayers: Array<any>;
  layerWithAxes: FeatureGroup = new FeatureGroup;
  layerWithMeasurements: FeatureGroup = new FeatureGroup;
  drawnElements: FeatureGroup = new FeatureGroup;

  layersControl: LayersControl = { baseLayers: {}, groupedLayers: {} };
  minimaplayersControl: LayersControl = { baseLayers: {}, overlays: {} };
  projectionControl: Projection;

  minimap;

  defaultIcon = new Icon({
    iconUrl: 'assets/icons/selectedPoint.svg',
    iconSize: [30, 30]
  });

  undefinedIcon = new Icon({
    iconUrl: 'assets/icons/clickedIssue.png',
    iconSize: [40, 40],
  });

  obsIcon = new Icon({
    iconUrl: 'assets/icons/selected issue.svg',
    iconSize: [40, 40],
  });

  snackbarDuration = 700;

  constructor(private storedIssuesService: StoredIssuesService,
    private zone: NgZone,
  ) { }

  groupedLayers2Geojson(transformedLayers) {
    // proj4.defs('EPSG:4326');
    this.transformedLayers = transformedLayers;
    Object.keys(this.transformedLayers).forEach((key: string) => {
      if (!!!this.layersControl.groupedLayers[key]) {
        this.layersControl.groupedLayers[key] = {};
      }
      if (!!!this.minimaplayersControl.overlays[key]) {
        this.minimaplayersControl.overlays[key] = {};
      }
      Object.keys(this.transformedLayers[key]).forEach((key1: string, index2: number) => {
        this.layersControl.groupedLayers[key][key1] = this.toGeoJson(this.transformedLayers[key][key1], index2, false);
        this.minimaplayersControl.overlays[key][key1] = this.toGeoJson(this.transformedLayers[key][key1], index2, true);
      });
    });
  }


  toGeoJson(feature: Proj4GeoJSONFeature, colourIndex: number, minimap: boolean) {
    return minimap ? geoJSON(feature, {
      style: { color: CSS_COLOR_NAMES[colourIndex], weight: 2 }, pointToLayer: (feature: any, latlng: LatLng) => {
        return new CircleMarker(latlng, { radius: 0.5, fillOpacity: 1 });
      },
      coordsToLatLng: this.coordsToLatLng.bind(this),
      onEachFeature: this.onEachFeature.bind(this)
    }) : geoJSON(feature, {
      style: { color: CSS_COLOR_NAMES[colourIndex] }, pointToLayer: (feature: any, latlng: any) => {
        // Text property which only .csv text has
        if (!!feature.properties.Text) {
          let label = String(feature.properties.Text);
          let rotation = Number(feature.properties.rotationAngle);
          let marker = new Marker(latlng, {
            // @ts-ignore
            icon: new DivIcon.customDivIconColor({
              iconSize: new Point(0, 0),
              html: label,
              color: CSS_COLOR_NAMES[colourIndex]
            }),
            rotationAngle: rotation
          })
          return marker;
        } else {
          return new CircleMarker(latlng, { radius: 0.5, fillOpacity: 1 });
        }
      },
      coordsToLatLng: this.coordsToLatLng.bind(this),
      onEachFeature: this.onEachFeature.bind(this),
    });
  }

  // Leaflet uses LatLng, not LngLat, this function transforms coords
  coordsToLatLng(coords: number[]) {
    return [coords[1], coords[0]];
  }

  onEachFeature(feature: IGeoJson, layer: Layer) {
    layer.on('click', <LeafletMouseEvent>(e: any) => {
      const flyTo: LatLng = !!feature.geometry.geometries ?
        latLng(feature.geometry.geometries[0].coordinates[0][1], feature.geometry.geometries[0].coordinates[0][0]) :
        !!feature.geometry.coordinates ? feature.geometry.type === 'Point' ?
          latLng(feature.geometry.coordinates[1], feature.geometry.coordinates[0]) :
          latLng(feature.geometry.coordinates[0][1], feature.geometry.coordinates[0][0]) :
          layer['_latlngs'];
      if (this.map.getZoom() < 0) {
        this.map.setView(flyTo, 2, { animate: false });
      }
      if (this.placeOnCenter) {
        this.zone.run(() => {
          this.placeOnCenter = false;
          this.map.pm.disableDraw('Marker');
          let featureCenter = center(feature);
          let latlng = [featureCenter.geometry.coordinates[1], featureCenter.geometry.coordinates[0]]
          let centeredMarker: Marker = new Marker(latLng(latlng[0], latlng[1]), { icon: this.defaultIcon });
          // @ts-ignore
          let layer = centeredMarker.pm._layer;
          centeredMarker.addTo(this.map);
          this.layerWithIssue = layer;
          let newIssue: Issue = {
            _id: undefined,
            discipline: undefined,
            subdiscipline: undefined,
            verification: undefined,
            coordinates: centeredMarker.getLatLng(),
            accept: false
          };
          this.storedIssuesService.selectIssue(newIssue);
          this.storedIssuesService.addEditMode();
        })
      }
    })
  }

  setDrawnIssues(issues) {
    if (!!issues) {
      this.drawnElements = geoJSON(issues).addTo(this.map);
      Object.keys(this.drawnElements['_layers']).forEach((key: string) => {
        this.drawnElements['_layers'][key].setIcon(this.defaultIcon);
        this.storedIssuesService.saveIssue({
          ...this.drawnElements['_layers'][key].feature.properties.issue
        });
        this.storedIssuesService.selectIssue(null);
        this.storedIssuesService.removeEditMode();
      });
    } else {
      this.drawnElements.addTo(this.map);
    }
  }

  setLayerWithAxes() {
    this.layerWithAxes.addTo(this.map);
    console.log(this.layerWithAxes);
    // @ts-ignore
    (this.layerWithAxes as FeatureGroup).pm.enable();
    this.layerWithAxes.on('click', (layerEvent: any) => {
      this.map.once('contextmenu', (e: LeafletKeyboardEvent) => {
        this.layerWithAxes.eachLayer((layer) => {
          if (!!(layer as any).options.hasOwnProperty('id')) {
            if ((layer as any).options.id === layerEvent.layer.options.id) {
              this.layerWithAxes.removeLayer(layer);
            }
          } else if (!!(layer as any).options.patterns) {
            if ((layer as any).options.patterns[0].symbol.options.pathOptions.id === layerEvent.layer.options.id) {
              this.layerWithAxes.removeLayer(layer);
            }
          }
        });
      });
    });

  }

  setDrawnIssuesClickListener() {
    this.drawnElements.on('click', (e: any) => {
      // Edit mode, if observation, add measurement.
      if (this.storedIssuesService.editMode$.getValue()) {
        this.zone.run(() => {
          if (this.storedIssuesService.selectedIssue$.getValue().verification.hasOwnProperty('measurement')) {
            let latlngs = (polyline((this.projectionControl)._axesArray.xAxis) as any)._latlngs;
            let origin = center(points([[latlngs[0].lat, latlngs[0].lng], [latlngs[1].lat, latlngs[1].lng]]));
            const selectedIssue = (this.storedIssuesService.selectedIssue$.getValue() as Issue).coordinates
            this.layerWithAxes.addLayer(polyline([origin.geometry.coordinates, selectedIssue], {
              id: this.storedIssuesService.selectedIssue$.getValue()._id,
              color: 'aqua'
            }));
            this.layerWithAxes.addLayer(new L.polylineDecorator([origin.geometry.coordinates, [selectedIssue.lat, selectedIssue.lng]], {
              // @ts-ignore
              patterns: [
                {
                  offset: '100%',
                  repeat: 0,
                  // @ts-ignore
                  symbol: L.Symbol.arrowHead({
                    pixelSize: 30, polygon: false, pathOptions: {
                      stroke: true, color: 'aqua',
                      id: this.storedIssuesService.selectedIssue$.getValue()._id,
                    }
                  })
                }
              ],
            }));
            this.projectionControl._closePath();
            let centerAxis = center(points([[origin.geometry.coordinates[0], origin.geometry.coordinates[1]], [selectedIssue.lat, selectedIssue.lng]])).geometry.coordinates
            // this.layerWithAxes.addLayer(polyline((this.projectionControl as any)._axesArray.xAxis));
            // this.layerWithAxes.addLayer(polyline((this.projectionControl as any)._axesArray.yAxis));
            let distance = this.projectionControl._distance
            if (this.storedIssuesService.measurementUnit$.value === 'inches') {
              distance = distance * this.metersToInches;
            }
            this.storedIssuesService.saveMeasurement(distance);
            this.layerWithAxes.addLayer(new Marker(centerAxis, {
              // @ts-ignore
              id: this.storedIssuesService.selectedIssue$.getValue()._id,
              icon: divIcon({
                iconSize: new Point(40, 14),
                html: (distance).toFixed(2).toString() + ' ' + this.storedIssuesService.measurementUnit$.value,
                className: 'measurement-background',
              }),
            }));
            this.projectionControl._closePath();
            const controlLines = this.getControlLines(this.layersControl.groupedLayers['$']['$_Issue Map'])
            const closestSegmPoints = this.findClosestControlLines(controlLines, point(selectedIssue.lat, selectedIssue.lng));
            this.addDistanceFromControlLines(closestSegmPoints, point(selectedIssue.lat, selectedIssue.lng));
          };
        });
        // @ts-ignore
        this.layerWithMeasurements.pm.toggleEdit();
      } else {
        // Select Issue and zoom to it.
        this.zone.run(() => {
          this.storedIssuesService.selectIssue(e.layer.feature.properties.issue);
          if (this.map.getZoom() < -2)
            this.map.setView(this.storedIssuesService.selectedIssue$.getValue().coordinates, 2);
        });
      };
    });
  }

  addDistanceFromControlLines(closestSegmPoints, selectedIssue: Point) {
    for (let i = 0; i < 2; i++) {
      const latlng = new LatLng((closestSegmPoints[i].layer as Layer).feature.geometry.coordinates[0][1], (closestSegmPoints[i].layer as Layer).feature.geometry.coordinates[0][0]);
      // @ts-ignore
      closestSegmPoints[i].projectedDistance = this.calculateLength(new Point(latlng.lat, latlng.lng), new Point(closestSegmPoints[i].projection.lat, closestSegmPoints[i].projection.lng))

      let centerAxeMeasurement = center(this.addProjectionLines(point(selectedIssue.x, selectedIssue.y), point(closestSegmPoints[i].projection.lat, closestSegmPoints[i].projection.lng)).toGeoJSON())
      if (this.storedIssuesService.measurementUnit$.value === 'inches') {
        closestSegmPoints[i].projectedDistance *= this.metersToInches;
      }
      this.layerWithMeasurements.addLayer(
        new Marker(new LatLng(centerAxeMeasurement.geometry.coordinates[1], centerAxeMeasurement.geometry.coordinates[0]), {
          // @ts-ignore
          id: this.storedIssuesService.selectedIssue$.getValue()._id,
          icon: divIcon({
            iconSize: new Point(40, 14),
            html: closestSegmPoints[i].projectedDistance.toFixed(2).toString(),
            className: 'measurement-background',
          }),
        })
      )
      this.layerWithMeasurements.addTo(this.map);
    }
  }

  findClosestControlLines(controlLines, selectedIssue: Point) {
    console.log(controlLines);
    let closestSegmPoints = [];
    for (let key in controlLines) {
      let closestOnSameSlope: { distance: number, projection: LatLng, layer: Layer };
      controlLines[key].map((layer: Layer) => {
        // @ts-ignore
        let projection: LatLng = L.GeometryUtil.closestOnSegment(this.map, [selectedIssue.x, selectedIssue.y], [layer.feature.geometry.coordinates[0][1], layer.feature.geometry.coordinates[0][0]], [layer.feature.geometry.coordinates[1][1], layer.feature.geometry.coordinates[1][0]]);
        let distance = L.GeometryUtil.length([projection, new LatLng(selectedIssue.x, selectedIssue.y)])
        if (!closestOnSameSlope) {
          closestOnSameSlope = { distance, projection, layer };
        } else if (closestOnSameSlope.distance > distance) {
          closestOnSameSlope = { distance, projection, layer };

        }
      })
      closestSegmPoints.push(closestOnSameSlope);
    }
    return closestSegmPoints.sort((a, b) => a.distance - b.distance)
  }

  getControlLines(controlLineFeatGroup: FeatureGroup) {
    let controlLines = {};
    controlLineFeatGroup.eachLayer((layer: Layer) => {
      if (layer.feature.geometry.type === 'LineString' && layer.feature.geometry.coordinates.length === 2) {
        let y1 = layer.feature.geometry.coordinates[0][1];
        let y2 = layer.feature.geometry.coordinates[1][1];
        let x1 = layer.feature.geometry.coordinates[0][0]
        let x2 = layer.feature.geometry.coordinates[1][0]
        const slope = ((y2 - y1) / (x2 - x1)).toFixed(2);
        if (!!!controlLines[slope]) {
          controlLines[slope] = []
        }
        controlLines[slope].push(layer)
        //new Marker({ lat: layer.feature.geometry.coordinates[0][1], lng: layer.feature.geometry.coordinates[0][0] }).addTo(this.map);
      }
    })
    return controlLines;
  }

  addProjectionLines(point1: Point, point2: Point): Polyline {
    let projectionLineInverse = new Polyline([[point1.x, point1.y], [point2.x, point2.y]], {
      id: this.storedIssuesService.selectedIssue$.getValue()._id,
      color: 'red'
    })
    let projectionLine = new Polyline([[point2.x, point2.y], [point1.x, point1.y]], {
      id: this.storedIssuesService.selectedIssue$.getValue()._id,
      color: 'red'
    })
    this.layerWithAxes.addLayer(projectionLine)
    this.layerWithAxes.addLayer(new L.polylineDecorator(projectionLine, {
      // @ts-ignore
      patterns: [
        {
          offset: '100%',
          repeat: 0,
          // @ts-ignore
          symbol: L.Symbol.arrowHead({
            pixelSize: 30, polygon: false, pathOptions: {
              stroke: true, color: 'red',
              id: this.storedIssuesService.selectedIssue$.getValue()._id,
            }
          })
        }
      ],
    }));
    this.layerWithAxes.addLayer(new L.polylineDecorator(projectionLineInverse, {
      // @ts-ignore
      patterns: [
        {
          offset: '100%',
          repeat: 0,
          // @ts-ignore
          symbol: L.Symbol.arrowHead({
            pixelSize: 30, polygon: false, pathOptions: {
              stroke: true, color: 'red',
              id: this.storedIssuesService.selectedIssue$.getValue()._id,
            }
          })
        }
      ],
    }));
    return projectionLine;
  }

  initializeLayerFeatureGroup(layersControl: LayersControl) {
    const layerArr: Layer[] = [];
    Object.keys(layersControl.overlays).forEach((key: string) => {
      Object.keys(layersControl.overlays[key]).forEach((key1: string) => {
        layerArr.push(layersControl.overlays[key][key1]);
      });
    });

    return layerArr;
  }

  initializeMap(snackbar: MatSnackBar) {
    let layerArr = this.initializeLayerFeatureGroup(this.minimaplayersControl)
    this.layersFeatureGroup = new FeatureGroup(layerArr);
    this.map.setMaxBounds(
      latLngBounds(this.layersFeatureGroup.getBounds().getSouthWest(), this.layersFeatureGroup.getBounds().getNorthEast()).pad(1)
    );
    if (!!this.drawnElements.getLayers().length) {
      this.map.setView(this.drawnElements.getBounds().getCenter(), 2);
    } else {
      // @ts-ignore
      this.map.setView(this.layersFeatureGroup.getBounds().getNorthEast(), -7);
    }
    this.minimapFeatureGroup = new FeatureGroup(layerArr);
    this.minimap = new Control.MiniMap(this.minimapFeatureGroup,
      { toggleDisplay: true, position: 'bottomright', width: 450, height: 300, zoomLevelFixed: 0 }).addTo(this.map);
    //this.projectionControl = control.projection().addTo(this.map);
    this.projectionControl = new Projection(snackbar).addTo(this.map);
    this.projectionControl.getContainer().remove();

    // Move controls to position.
    let rulerDOM = document.getElementById('ruler');
    rulerDOM.appendChild(this.projectionControl.onAdd(this.map));
    this.map.zoomControl.getContainer().remove();
    rulerDOM.appendChild(this.map._controlContainer.firstChild);
    rulerDOM.appendChild(this.map.zoomControl.onAdd(this.map));

    let geoman = this.map.pm.addControls({
      position: 'topleft',
      drawPolyline: false,
      drawPolygon: false,
      drawRectangle: false,
      drawCircleMarker: false,
      drawCircle: false,
      editMode: false,
      dragMode: false,
      cutPolygon: false,
      drawMarker: true,
      removalMode: false,
    });

    this.toggleLayers(this.transformedLayers);
    control.groupedLayers({}, this.layersControl.groupedLayers as FeatureGroup, { groupCheckboxes: true }).addTo(this.map);
    this.enableSnapping();

    /**
     * MOVING BETWEEN ISSUES WITH A AND D
     */
    this.map.on('keypress', (e: LeafletKeyboardEvent) => {
      if (e.originalEvent.key === 'c') {
        this.zone.run(() => {
          this.placeOnCenter = !this.placeOnCenter;
          this.placeOnCenter ? snackbar.open('Issue centering enabled', '', {
            duration: this.snackbarDuration,
            politeness: 'polite',
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['snackbar-primary']

          }) : snackbar.open('Issue centering disabled', '', {
            duration: this.snackbarDuration,
            politeness: 'assertive',
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['snackbar-warn']
          });
        });
      }
      if (this.storedIssuesService.issues$.value.length > 0) {
        // a === left
        if (e.originalEvent.which === 97) {
          if (!!this.storedIssuesService.selectedIssue$.value) {
            if (this.storedIssuesService.selectedIssue$.value === this.storedIssuesService.issues$.value[0]) {
              this.zone.run(() => {
                let lastIssue = this.storedIssuesService.issues$.value[this.storedIssuesService.issues$.value.length - 1];
                this.storedIssuesService.selectIssue(lastIssue);
                this.map.setView(this.storedIssuesService.selectedIssue$.value.coordinates, 2);
              });
            } else {
              this.zone.run(() => {
                let issueBeforeIdx = this.storedIssuesService.issues$.value.findIndex(issue => issue === this.storedIssuesService.selectedIssue$.value) - 1;
                let issueBefore = this.storedIssuesService.issues$.value[issueBeforeIdx];
                this.storedIssuesService.selectIssue(issueBefore);
                this.map.setView(this.storedIssuesService.selectedIssue$.value.coordinates, 2);
              });
            }
          } else {
            this.storedIssuesService.selectIssue(this.storedIssuesService.issues$.value[0]);
            this.map.setView(this.storedIssuesService.selectedIssue$.value.coordinates, 2);

          }
          // d === right
        } else if (e.originalEvent.which === 100) {
          if (!!this.storedIssuesService.selectedIssue$.value) {
            if (this.storedIssuesService.selectedIssue$.value._id === this.storedIssuesService.issues$.value[this.storedIssuesService.issues$.value.length - 1]._id) {
              this.zone.run(() => {
                let firstIssue = this.storedIssuesService.issues$.value[0];
                this.storedIssuesService.selectIssue(firstIssue);
                this.map.setView(this.storedIssuesService.selectedIssue$.value.coordinates, 2);
              });
            } else {
              this.zone.run(() => {
                let issueAfterIdx = this.storedIssuesService.issues$.value.findIndex(issue => issue === this.storedIssuesService.selectedIssue$.value) + 1;
                let issueAfter = this.storedIssuesService.issues$.value[issueAfterIdx];
                this.storedIssuesService.selectIssue(issueAfter);
                this.map.setView(this.storedIssuesService.selectedIssue$.value.coordinates, 2);
              });
            }
          } else {
            this.storedIssuesService.selectIssue(this.storedIssuesService.issues$.value[this.storedIssuesService.issues$.value.length - 1]);
            this.map.setView(this.storedIssuesService.selectedIssue$.value.coordinates, 2);
          }
        }
      }
    });
  }

  toggleLayers(transformedLayers) {
    Object.keys(transformedLayers).forEach((key: string, index) => {
      Object.keys(transformedLayers[key]).forEach((key1: string, index2: number) => {
        if (key === '$') {
          this.map.addLayer(this.layersControl.groupedLayers[key][key1]);
        }
      });
    });
  }

  enableSnapping() {
    const options = {
      snappable: false, tooltips: true, templineStyle: { color: 'white', weight: 1 }, hintlineStyle: {
        color: 'white',
        dashArray: [5, 5],
        weight: 1
      },
      markerStyle: { icon: this.defaultIcon }
    };
    this.map.pm.disableDraw('Rectangle');
    this.map.pm.disableDraw('Polygon');
    this.map.pm.disableDraw('Line');
    this.map.pm.enableDraw('Marker', options);
    this.map.pm.disableDraw('Marker');
  }

  removeIssue(layer: Layer) {
    this.drawnElements.removeLayer(layer);
  }

  calculateLength(point: Point, point2: Point) {
    let length = Math.sqrt(Math.pow(point.x - point2.x, 2.0) + Math.pow(point.y - point2.y, 2.0));
    return length;
  };
}
