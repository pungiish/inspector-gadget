import { TestBed } from '@angular/core/testing';

import { StoredIssuesService } from './stored-issues.service';

describe('StoredIssuesService', () => {
  let service: StoredIssuesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoredIssuesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
