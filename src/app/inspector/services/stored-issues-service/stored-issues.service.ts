import { Injectable } from '@angular/core';
import { StoredIssuesDataService } from './data.stored-issues.service';
import { Issue, Verification } from 'shared/models/models';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoredIssuesService implements StoredIssuesDataService {
  readonly issues$: BehaviorSubject<Issue[]> = new BehaviorSubject([]);
  readonly editMode$: BehaviorSubject<boolean> = new BehaviorSubject(true);
  readonly selectedIssue$: BehaviorSubject<any> = new BehaviorSubject(null);
  readonly measurementUnit$: BehaviorSubject<Measurement> = new BehaviorSubject('meters');
  selectedIssueObs$: Observable<Issue>;
  editModeObs$: Observable<boolean>;
  issuesObs$: Observable<Issue[]>;
  measurementObs$: Observable<Measurement>;
  constructor() {
    this.issuesObs$ = this.issues$.asObservable();
    this.editModeObs$ = this.editMode$.asObservable();
    this.selectedIssueObs$ = this.selectedIssue$.asObservable();
    this.measurementObs$ = this.measurementUnit$.asObservable();
  }

  private get issues(): Issue[] {
    return this.issues$.getValue();
  }

  setIssues(nextState: Issue[]): void {
    this.issues$.next(nextState);
  }

  saveIssue(issue: Issue): Issue[] {
    // Get the ID of the last issue in the array and increment it
    if (this.issues.length > 0) {
      issue._id = this.issues[this.issues.length - 1]._id + 1;
    } else {
      issue._id = 0;
    }
    console.log(issue);
    if (!this.issues.find(storedIssue => issue._id === storedIssue._id)) {
      console.log('pushing new issue');
      this.setIssues(
        [...this.issues, issue]);
    } else {
      console.log('issue with this ID already exists!');
    }
    this.selectIssue(issue);
    return this.issues;
  }

  editIssue(issue: Issue): Issue {
    let index = this.issues.findIndex(storedIssue => issue._id === storedIssue._id);
    if (index !== -1) {
      this.issues[index] = issue;
      this.setIssues(this.issues);
    } else {
      console.log('The issue with this ID doesn\'t exist yet!');
    }
    this.selectIssue(issue);
    return issue;
  }

  removeIssue(id: number): void {
    let index = this.issues.findIndex(storedIssue => id === storedIssue._id);
    if (index === -1) {
      console.log('This issue doesnt exist yet!');
    } else {
      this.setIssues(this.issues.filter(storedIssue => id !== storedIssue._id));
    }
    this.selectIssue(null);
    return;
  }

  private get editMode(): boolean {
    return this.editMode$.getValue();
  }

  setEditMode(nextState: boolean): void {
    this.editMode$.next(nextState);
  }

  addEditMode() {
    this.setEditMode(true);
  }

  removeEditMode() {
    this.setEditMode(false);
  }

  private get selectedIssue(): Issue {
    return this.selectedIssue$.getValue();
  }

  selectIssue(issue: Issue): void {
    console.log(this.editMode);
    this.selectedIssue$.next(issue);
  }

  saveMeasurement(measurement: number): void {
    if ((this.selectedIssue.verification as Verification).hasOwnProperty('measurement')) {
      if (this.editMode) {
        console.log('save Measurement');
        console.log(measurement)
        this.selectedIssue.verification;
        this.editIssue({
          ...this.selectedIssue,
          verification: {
            classification: (this.selectedIssue.verification as Verification).classification,
            subclassification: (this.selectedIssue.verification as Verification).subclassification,
            comment: (this.selectedIssue.verification as Verification).comment,
            measurement: measurement
          }
        });
      }
    }
  }

  private get measurementUnit(): Measurement {
    return this.measurementUnit$.getValue();
  }

  setMeasurementUnit(string: Measurement): void {
    this.measurementUnit$.next(string);
  }

  resetData(): void {
    this.setIssues([]);
    console.log(this.issues)
  }
}

export type Measurement = 'meters' | 'inches';
