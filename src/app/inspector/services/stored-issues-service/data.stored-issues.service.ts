import { Issue } from 'shared/models/models';
import { Observable, BehaviorSubject } from 'rxjs';
import { Measurement } from './stored-issues.service';
export interface StoredIssuesDataService {
  issues$: BehaviorSubject<Issue[]>;
  issuesObs$: Observable<Issue[]>;
  editMode$: BehaviorSubject<boolean>;
  editModeObs$: Observable<boolean>;
  selectedIssue$: BehaviorSubject<Issue>;
  measurementUnit$: BehaviorSubject<Measurement>;
  saveIssue(issue: Issue): Issue[];
  editIssue(issue: Issue): Issue;
  removeIssue(id: number): void;
  saveMeasurement(measurement: number): void;
}
