import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './components/main/main.component';
import { InspectorRoutingModule } from './inspector-routing/inspector-routing.module';
import { PlanningComponent } from './components/planning/planning.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { HttpClientModule } from '@angular/common/http';
import '../shared/helpers/arrayExtensions';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'shared/shared.module';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';


import { IssueViewComponent } from './components/view/components/issue-view/issue-view.component';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';


import { IssuesSidebarComponent } from './components/issues-sidebar/issues-sidebar.component';
import { MainIssueViewComponent } from './components/view/main/main-issue-view/main-issue-view.component';
import { ObservationIssueViewComponent } from './components/view/components/observation-issue-view/observation-issue-view.component';
import { ObservationIssueEditComponent } from './components/edit/components/observation-issue-edit/observation-issue-edit.component';
import { IssueEditComponent } from './components/edit/components/issue-edit/issue-edit.component';
import { MainIssueEditComponent } from './components/edit/main/main-issue-edit/main-issue-edit.component';

const MATERIAL_MODULES = [
  MatSelectModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatTabsModule,
  MatExpansionModule,
  MatListModule,
  MatTooltipModule,
  MatSnackBarModule
];

@NgModule({
  declarations: [MainComponent, PlanningComponent, IssueViewComponent, IssuesSidebarComponent, MainIssueViewComponent, ObservationIssueViewComponent, ObservationIssueEditComponent, IssueEditComponent, MainIssueEditComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    InspectorRoutingModule,
    LeafletModule,
    SharedModule,
    ReactiveFormsModule,
    MATERIAL_MODULES
  ]
})
export class InspectorModule { }
