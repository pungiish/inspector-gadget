import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from '../components/main/main.component';
import { PlanningComponent } from '../components/planning/planning.component';

const inspectorRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [{
      path: '',
      pathMatch: 'full',
      redirectTo: 'planning'
    }, {
      path: 'planning',
      component: PlanningComponent
    }]
  }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(inspectorRoutes),
  ],
  exports: [RouterModule]
})
export class InspectorRoutingModule { }
