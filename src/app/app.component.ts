import { Component } from '@angular/core';
import { ApiProjectDataService } from 'shared/services/api-data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'inspector-gadget';

  constructor(
    public projectDataService: ApiProjectDataService) {
      this.projectDataService.isLoading$.next(false);
  }
}
