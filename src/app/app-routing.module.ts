import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'upload-project'
  },
  {
    path: 'upload-project',
    loadChildren: () => import('./project-selection/project-selection.module').then(m => m.ProjectSelectionModule)
  }, {
    path: 'project',
    loadChildren: () => import('./inspector/inspector.module').then(m => m.InspectorModule)
  }
];
// { enableTracing: true }
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
