export const environment = {
  production: true,
  url: `http://ogre.adc4gis.com/convert`,
  serverUrl: `https://api-qa.dronomy.com`
};
